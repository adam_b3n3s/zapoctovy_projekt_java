# Zápočtový projekt šachy

## Co je projekt zač?

Tento projekt jsou šachy, určené na hraní na PC. Módy, které je schopen člověk hrát jsou:

- Proti botovi.
- Přes síť, jen je potřeba sprovoznit server.
- Hot seat, dva hráči na jednom počítači.

## Swing

Šachy jsou tvořeny v Java Swing.
