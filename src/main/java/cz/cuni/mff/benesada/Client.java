/**
 * The {@code Client} class represents a client in a chess-playing system.
 * It contains a static boolean variable {@code isClientTurn} indicating
 * whether it is the client's turn to make a move.
 * 
 * This class is a part of the chess-playing system and is used to manage
 * the turn-based gameplay.
 */
package cz.cuni.mff.benesada;

public class Client {
    /**
     * A static boolean variable indicating whether it is the client's turn
     * to make a move. If {@code isClientTurn} is true, it is the client's turn;
     * otherwise, it is not the client's turn.
     */
    public static boolean isClientTurn = false;
}
