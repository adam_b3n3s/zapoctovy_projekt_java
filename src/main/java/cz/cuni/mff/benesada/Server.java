package cz.cuni.mff.benesada;

import java.io.*;
import java.net.*;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The main server class responsible for handling client connections and managing game pairing.
 */
public class Server {

    private static final int port = 12345; // Change this to the desired port number
    private static final Map<String, Socket> waitingClients = new ConcurrentHashMap<>();

    /**
     * The main method that starts the server and listens for client connections.
     *
     * @param args Command-line arguments (not used in this implementation).
     */
    public static void main(String[] args) {
        try {
            try (ServerSocket serverSocket = new ServerSocket(port)) {
                System.out.println("Server is listening on port " + port);

                while (true) {
                    Socket clientSocket = serverSocket.accept();
                    System.out.println("Client connected: " + clientSocket.getInetAddress());

                    // Read the code sent by the client
                    BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    String code = reader.readLine();

                    if (waitingClients.containsKey(code)) {
                        // If a matching code is found, pair the clients and remove the waiting client
                        Socket waitingClient = waitingClients.remove(code);

                        System.out.println("Pairing clients " + clientSocket.getInetAddress() + " and "
                                + waitingClient.getInetAddress());

                        PrintWriter writerOne = new PrintWriter(clientSocket.getOutputStream(), true);
                        PrintWriter writerTwo = new PrintWriter(waitingClient.getOutputStream(), true);

                        startGame(writerOne, writerTwo);

                        Thread clientOneThread = new Thread(new ClientHandler(clientSocket, waitingClient));
                        Thread clientTwoThread = new Thread(new ClientHandler(waitingClient, clientSocket));

                        clientOneThread.start();
                        clientTwoThread.start();
                    } else {
                        // If no matching code is found, add the client to the waiting list
                        waitingClients.put(code, clientSocket);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts the game and informs the clients about their roles (first or second player).
     *
     * @param clientOneOutput PrintWriter for the first player.
     * @param clientTwoOutput PrintWriter for the second player.
     */
    public static void startGame(PrintWriter clientOneOutput, PrintWriter clientTwoOutput) {
        Random random = new Random();
        int randomNumber = random.nextInt(2);
        if (randomNumber == 0) {
            clientOneOutput.println(true);
            clientTwoOutput.println(false);
        } else {
            clientOneOutput.println(false);
            clientTwoOutput.println(true);
        }
        System.out.println("Game started!");
    }
}

/**
 * Handles communication between a client and its paired client.
 */
class ClientHandler implements Runnable {
    private final Socket clientSocket;
    private final Socket pairedClientSocket;

    /**
     * Constructor for the ClientHandler class.
     *
     * @param clientSocket The socket for the current client.
     * @param pairedClientSocket The socket for the paired client.
     */
    public ClientHandler(Socket clientSocket, Socket pairedClientSocket) {
        this.clientSocket = clientSocket;
        this.pairedClientSocket = pairedClientSocket;
    }

    /**
     * Runs the thread, continuously reading messages from the client and sending them to the paired client.
     */
    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter writer = new PrintWriter(pairedClientSocket.getOutputStream(), true);

            String message;

            while ((message = reader.readLine()) != null) {
                writer.println(message);
            }

            System.out.println("Connection closed for client: " + clientSocket.getInetAddress());
            clientSocket.close();
            pairedClientSocket.close();
        } catch (IOException e) {
            System.out.println("Client close the game: " + clientSocket.getInetAddress());
        }
    }
}
