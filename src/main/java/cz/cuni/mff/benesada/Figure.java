/**
 * The {@code Figure} class represents a chess piece in a chess-playing system.
 * It serves as the base class for specific chess piece types such as King, Queen,
 * Bishop, Knight, Rook, and Pawn.
 * 
 * This class provides functionalities common to all chess pieces, including their
 * position, color, image representation, and possible moves.
 */
package cz.cuni.mff.benesada;

import java.util.Arrays;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class Figure implements Cloneable {

    /**
     * Flag indicating whether possible moves for this figure have been set.
     */
    public boolean possibleMovesIsSet = false;

    /**
     * Flag indicating whether this figure can participate in castling.
     */
    public boolean canCastle = false;

    /**
     * String representing the user's choice during pawn promotion.
     */
    public String choose = "";

    /**
     * Array representing the last move of a pawn.
     */
    public static int[] lastMoveOfPawn = new int[2];

    /**
     * Path to the directory containing chess piece images.
     */
    public static String PATH_IMAGE = "./images/";

    /**
     * Numerical value assigned to the chess piece.
     */
    public int value = 0;

    /**
     * X-coordinate of the figure's position on the chessboard.
     */
    public int positionX;

    /**
     * Y-coordinate of the figure's position on the chessboard.
     */
    public int positionY;

    /**
     * String representing the color of the figure (either "white" or "black").
     */
    public String color;

    /**
     * 2D array representing the possible moves of the figure on the chessboard.
     */
    int[][] possibleMoves = new int[8][8];

    /**
     * Boolean indicating whether the figure is selected.
     */
    public Boolean selected;

    /**
     * BufferedImage representing the image of the chess piece.
     */
    private BufferedImage figureImage;

    /**
     * String representing the color of the bishop (used for special cases).
     */
    public String bishopColor;

    /**
     * String indicating whether the pawn has been promoted to another piece.
     */
    public String promoted = "";

    /**
     * Boolean flag indicating whether the chess piece images have been loaded.
     */
    public static Boolean figureImagesFlag = true;

    /**
     * Constructs a new Figure object with the specified position, color, and image path.
     * 
     * @param positionX The X-coordinate of the figure on the chessboard.
     * @param positionY The Y-coordinate of the figure on the chessboard.
     * @param color     The color of the figure ("white" or "black").
     * @param imagePath The path to the directory containing chess piece images.
     */
    public Figure(int positionX, int positionY, String color, String imagePath) {
        if (figureImagesFlag) {
            Main.loadImages();
            figureImagesFlag = false;
        }

        if (!(this instanceof Bishop)) {
            this.bishopColor = "";
        }
        if (this instanceof Rook || this instanceof King) {
            this.canCastle = true;
        }
        this.possibleMoves = new int[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this.possibleMoves[i][j] = 0;
            }
        }
        this.positionX = positionX;
        this.positionY = positionY;
        this.color = color;
        this.selected = false;
        if (color == "white") {

            imagePath = imagePath + "_white";

            switch (imagePath) {
                case "bishop_white" -> {
                    figureImage = Main.bishopWhite;
                    break;
                }
                case "king_white" -> {
                    figureImage = Main.kingWhite;
                    break;
                }
                case "knight_white" -> {
                    figureImage = Main.knightWhite;
                    break;
                }
                case "pawn_white" -> {
                    figureImage = Main.pawnWhite;
                    break;
                }
                case "queen_white" -> {
                    figureImage = Main.queenWhite;
                    break;
                }
                case "rook_white" -> {
                    figureImage = Main.rookWhite;
                    break;
                }
            }
        }
        if (color == "black") {
            imagePath = imagePath + "_black";
            switch (imagePath) {
                case "bishop_black" -> {
                    figureImage = Main.bishopBlack;
                    break;
                }
                case "king_black" -> {
                    figureImage = Main.kingBlack;
                    break;
                }
                case "knight_black" -> {
                    figureImage = Main.knightBlack;
                    break;
                }
                case "pawn_black" -> {
                    figureImage = Main.pawnBlack;
                    break;
                }
                case "queen_black" -> {
                    figureImage = Main.queenBlack;
                    break;
                }
                case "rook_black" -> {
                    figureImage = Main.rookBlack;
                    break;
                }
            }
        }

    }

    /**
     * Performs a shallow copy of the Figure object.
     * 
     * @return A cloned Figure object.
     */
    @Override
    public Object clone() {
        try {

            Figure clonedObject = (Figure) super.clone();
            clonedObject.value = this.value;

            clonedObject.positionX = this.positionX;
            clonedObject.positionY = this.positionY;
            clonedObject.color = this.color;
            clonedObject.canCastle = this.canCastle;
            clonedObject.bishopColor = this.bishopColor;

            return clonedObject;
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.getMessage());
        }
    }

    /**
     * Moves the current chess piece to the specified position on the chessboard,
     * updates the game state, and checks for checkmate or draw conditions.
     *
     * @param newPositionX        The X-coordinate of the new position on the chessboard.
     * @param newPositionY        The Y-coordinate of the new position on the chessboard.
     * @param wasTherePromotion   A string indicating whether there was a pawn promotion in the move.
     * @return                    A string representing the promoted piece if promotion occurred.
     */
    public String move(int newPositionX, int newPositionY, String wasTherePromotion) {

        promoted = "";
        for (Figure figure : Main.figureList) {
            figure.possibleMovesIsSet = false;
            if (figure.positionX == newPositionX && figure.positionY == newPositionY) {
                Main.figureList.remove(figure);
                Bot.wasThereCaptureOrCheck = true;
                Main.fiftyMoveCounter = 0;
                Main.positionCountMapWhite = new HashMap<>();
                Main.positionCountMapBlack = new HashMap<>();
                break;
            }
        }

        Main.moveCounter++;

        setPosition(newPositionX, newPositionY, wasTherePromotion);

        String positionString = Arrays.deepToString(Main.figures);
        if (this.color == "white") {
            if (Main.positionCountMapWhite.containsKey(positionString)) {
                int count = Main.positionCountMapWhite.get(positionString);
                Main.positionCountMapWhite.put(positionString, count + 1);
            } else {
                Main.positionCountMapWhite.put(positionString, 1);
            }
        }
        if (this.color == "black") {
            if (Main.positionCountMapBlack.containsKey(positionString)) {
                int count = Main.positionCountMapBlack.get(positionString);
                Main.positionCountMapBlack.put(positionString, count + 1);
            } else {
                Main.positionCountMapBlack.put(positionString, 1);
            }
        }

        if (Main.kw.checkmate(Main.fiftyMoveCounter, Main.figures, Main.positionCountMapWhite,
                Main.positionCountMapBlack) == "y") {
            Main.end("black");
        }
        if (Main.kb.checkmate(Main.fiftyMoveCounter, Main.figures, Main.positionCountMapWhite,
                Main.positionCountMapBlack) == "y") {
            Main.end("white");
        }
        if (Main.kw.checkmate(Main.fiftyMoveCounter, Main.figures, Main.positionCountMapWhite,
                Main.positionCountMapBlack) == "d") {
            Main.end("draw");
        }
        if (Main.kb.checkmate(Main.fiftyMoveCounter, Main.figures, Main.positionCountMapWhite,
                Main.positionCountMapBlack) == "d") {
            Main.end("draw");
        }
        otherPlayerPlay();
        System.out.println("50 MOVES COUNTER: " + Main.fiftyMoveCounter);
        return promoted;
    }

    /**
     * Switches the turn to the other player, updates remaining time, and starts/stops timers based on game type.
     * If the current player is white, it switches to black, and vice versa.
     */
    public void otherPlayerPlay() {
        if (Main.whoPlays == "white") {
            Main.secondsRemainingBlack += Main.secondsPerMove;
            if (Main.gameType == 0 || Main.gameType == 1) {
                Main.timerBlack.start();
                Main.timerWhite.stop();
            }
            Main.whoPlays = "black";
        } else if (Main.whoPlays == "black") {
            Main.secondsRemainingWhite += Main.secondsPerMove;
            if (Main.gameType == 0 || Main.gameType == 1) {
                Main.timerWhite.start();
                Main.timerBlack.stop();
            }
            Main.whoPlays = "white";
        }
    }

    /**
     * Sets possible castling moves for the current figure, considering the given conditions.
     *
     * @param alsoWithCasteling A flag indicating whether to include castling moves in the possible moves.
     * @param figures The 2D array representing the current state of the chessboard.
     * @param mustCheckCheck A flag indicating whether to check for potential checks during castling.
     */
    public void castelingSetPossibleMoves(boolean alsoWithCasteling, Figure[][] figures,
            boolean mustCheckCheck) {
        if (this.canCastle == true && this.color == "white") {
            if (figures[0][0] != null && figures[0][0].canCastle == true) {
                if (figures[1][0] == null && figures[2][0] == null && figures[3][0] == null) {
                    if (mustCheckCheck == false) {
                        this.possibleMoves[2][0] = 1;
                    } else if (!checkIfOpponentSeesThisSquare(2, 0, figures, this.color)
                            && !checkIfOpponentSeesThisSquare(3, 0, figures, this.color)
                            && !checkIfOpponentSeesThisSquare(4, 0, figures, this.color)) {
                        this.possibleMoves[2][0] = 1;
                    }
                }
            }
        }
        if (this.canCastle == true && this.color == "white") {
            if (figures[7][0] != null && figures[7][0].canCastle == true) {
                if (figures[5][0] == null && figures[6][0] == null) {
                    if (mustCheckCheck == false) {
                        this.possibleMoves[6][0] = 1;
                    } else if (!checkIfOpponentSeesThisSquare(5, 0, figures, this.color)
                            && !checkIfOpponentSeesThisSquare(6, 0, figures, this.color)
                            && !checkIfOpponentSeesThisSquare(4, 0, figures, this.color)) {
                        this.possibleMoves[6][0] = 1;
                    }
                }
            }
        }
        if (this.canCastle == true && this.color == "black") {
            if (figures[0][7] != null && figures[0][7].canCastle == true) {
                if (figures[1][7] == null && figures[2][7] == null && figures[3][7] == null) {
                    if (mustCheckCheck == false) {
                        this.possibleMoves[2][7] = 1;
                    } else if (!checkIfOpponentSeesThisSquare(2, 7, figures, this.color)
                            && !checkIfOpponentSeesThisSquare(3, 7, figures, this.color)
                            && !checkIfOpponentSeesThisSquare(4, 7, figures, this.color)) {
                        this.possibleMoves[2][7] = 1;
                    }
                }
            }
        }
        if (this.canCastle == true && this.color == "black") {
            if (figures[7][7] != null && figures[7][7].canCastle == true) {
                if (figures[5][7] == null && figures[6][7] == null) {
                    if (mustCheckCheck == false) {
                        this.possibleMoves[6][7] = 1;
                    } else if (!checkIfOpponentSeesThisSquare(5, 7, figures, this.color)
                            && !checkIfOpponentSeesThisSquare(6, 7, figures, this.color)
                            && !checkIfOpponentSeesThisSquare(4, 7, figures, this.color)) {
                        this.possibleMoves[6][7] = 1;
                    }
                }
            }
        }
        // Sets possible castling moves based on the conditions provided.

        // @param alsoWithCasteling: A flag indicating whether to include castling moves in the possible moves.
        // @param figures: The 2D array representing the current state of the chessboard.
        // @param mustCheckCheck: A flag indicating whether to check for potential checks during castling.
    }

    /**
     * Sets possible moves for the King figure based on its resting moves (non-castling).
     *
     * @param alsoWithCasteling A flag indicating whether to include castling moves in the possible moves.
     * @param figures The 2D array representing the current state of the chessboard.
     * @param mustCheckCheck A flag indicating whether to check for potential checks during the moves.
     */
    public void kingRestSetPossibleMoves(boolean alsoWithCasteling, Figure[][] figures,
            boolean mustCheckCheck) {
        if (this.getPositionX() + 1 < 8 && this.getPositionY() + 1 < 8
                && (figures[this.getPositionX() + 1][this.getPositionY() + 1] == null
                        || figures[this.getPositionX() + 1][this.getPositionY() + 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() + 1, this.getPositionY() + 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 1] = 1;

            }
        }
        if (this.getPositionX() + 1 < 8 && this.getPositionY() - 1 >= 0
                && (figures[this.getPositionX() + 1][this.getPositionY() - 1] == null
                        || figures[this.getPositionX() + 1][this.getPositionY() - 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY() - 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() + 1, this.getPositionY() - 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY() - 1] = 1;
            }
        }
        if (this.getPositionX() - 1 >= 0 && this.getPositionY() + 1 < 8
                && (figures[this.getPositionX() - 1][this.getPositionY() + 1] == null
                        || figures[this.getPositionX() - 1][this.getPositionY() + 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() - 1, this.getPositionY() + 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 1] = 1;
            }
        }
        if (this.getPositionX() - 1 >= 0 && this.getPositionY() - 1 >= 0
                && (figures[this.getPositionX() - 1][this.getPositionY() - 1] == null
                        || figures[this.getPositionX() - 1][this.getPositionY() - 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY() - 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() - 1, this.getPositionY() - 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY() - 1] = 1;
            }
        }
        if (this.getPositionX() + 1 < 8
                && (figures[this.getPositionX() + 1][this.getPositionY()] == null
                        || figures[this.getPositionX() + 1][this.getPositionY()].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY()] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() + 1, this.getPositionY(), figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY()] = 1;
            }
        }
        if (this.getPositionX() - 1 >= 0
                && (figures[this.getPositionX() - 1][this.getPositionY()] == null
                        || figures[this.getPositionX() - 1][this.getPositionY()].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY()] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() - 1, this.getPositionY(), figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY()] = 1;
            }
        }
        if (this.getPositionY() + 1 < 8
                && (figures[this.getPositionX()][this.getPositionY() + 1] == null
                        || figures[this.getPositionX()][this.getPositionY() + 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX()][this.getPositionY() + 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX(), this.getPositionY() + 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX()][this.getPositionY() + 1] = 1;
            }
        }
        if (this.getPositionY() - 1 >= 0
                && (figures[this.getPositionX()][this.getPositionY() - 1] == null
                        || figures[this.getPositionX()][this.getPositionY() - 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX()][this.getPositionY() - 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX(), this.getPositionY() - 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX()][this.getPositionY() - 1] = 1;
            }
        }

        // @param alsoWithCasteling: A flag indicating whether to include castling moves in the possible moves.
        // @param figures: The 2D array representing the current state of the chessboard.
        // @param mustCheckCheck: A flag indicating whether to check for potential checks during the moves.
    }

    public void knightSetPossibleMoves(boolean alsoWithCasteling, Figure[][] figures, boolean mustCheckCheck) {
        if (this.getPositionX() + 2 < 8 && this.getPositionY() + 1 < 8
                && (figures[this.getPositionX() + 2][this.getPositionY() + 1] == null
                        || figures[this.getPositionX() + 2][this.getPositionY() + 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() + 2][this.getPositionY() + 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() + 2, this.getPositionY() + 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() + 2][this.getPositionY() + 1] = 1;
            }
        }
        if (this.getPositionX() + 2 < 8 && this.getPositionY() - 1 >= 0
                && (figures[this.getPositionX() + 2][this.getPositionY() - 1] == null
                        || figures[this.getPositionX() + 2][this.getPositionY() - 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() + 2][this.getPositionY() - 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() + 2, this.getPositionY() - 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() + 2][this.getPositionY() - 1] = 1;
            }
        }
        if (this.getPositionX() - 2 >= 0 && this.getPositionY() + 1 < 8
                && (figures[this.getPositionX() - 2][this.getPositionY() + 1] == null
                        || figures[this.getPositionX() - 2][this.getPositionY() + 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() - 2][this.getPositionY() + 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() - 2, this.getPositionY() + 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() - 2][this.getPositionY() + 1] = 1;
            }
        }
        if (this.getPositionX() - 2 >= 0 && this.getPositionY() - 1 >= 0
                && (figures[this.getPositionX() - 2][this.getPositionY() - 1] == null
                        || figures[this.getPositionX() - 2][this.getPositionY() - 1].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() - 2][this.getPositionY() - 1] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() - 2, this.getPositionY() - 1, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() - 2][this.getPositionY() - 1] = 1;
            }
        }
        if (this.getPositionX() + 1 < 8 && this.getPositionY() + 2 < 8
                && (figures[this.getPositionX() + 1][this.getPositionY() + 2] == null
                        || figures[this.getPositionX() + 1][this.getPositionY() + 2].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 2] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() + 1, this.getPositionY() + 2, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 2] = 1;
            }
        }
        if (this.getPositionX() + 1 < 8 && this.getPositionY() - 2 >= 0
                && (figures[this.getPositionX() + 1][this.getPositionY() - 2] == null
                        || figures[this.getPositionX() + 1][this.getPositionY() - 2].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY() - 2] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() + 1, this.getPositionY() - 2, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() + 1][this.getPositionY() - 2] = 1;
            }
        }
        if (this.getPositionX() - 1 >= 0 && this.getPositionY() + 2 < 8
                && (figures[this.getPositionX() - 1][this.getPositionY() + 2] == null
                        || figures[this.getPositionX() - 1][this.getPositionY() + 2].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 2] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() - 1, this.getPositionY() + 2, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 2] = 1;
            }
        }
        if (this.getPositionX() - 1 >= 0 && this.getPositionY() - 2 >= 0
                && (figures[this.getPositionX() - 1][this.getPositionY() - 2] == null
                        || figures[this.getPositionX() - 1][this.getPositionY() - 2].color != this.color)) {
            if (mustCheckCheck == false) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY() - 2] = 1;
            } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                    this.getPositionY(), this.getPositionX() - 1, this.getPositionY() - 2, figures,
                    this.color)) {
                this.possibleMoves[this.getPositionX() - 1][this.getPositionY() - 2] = 1;
            }
        }
        // @param alsoWithCasteling: A flag indicating whether to include castling moves in the possible moves.
        // @param figures: The 2D array representing the current state of the chessboard.
        // @param mustCheckCheck: A flag indicating whether to check for potential checks during the moves.
    }

    /**
     * Sets possible moves for a figure with diagonal movement behavior (e.g., Bishop and Queen).
     *
     * @param alsoWithCasteling A flag indicating whether to include castling moves in the possible moves.
     * @param figures The 2D array representing the current state of the chessboard.
     * @param mustCheckCheck A flag indicating whether to check for potential checks during the moves.
     */
    public void bishopLikeSetPossibleMoves(boolean alsoWithCasteling, Figure[][] figures,
            boolean mustCheckCheck) {
        int x = this.getPositionX();
        int y = this.getPositionY();
        while (x + 1 < 8 && y + 1 < 8) {
            if (figures[x + 1][y + 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x + 1][y + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x + 1, y + 1, figures,
                        this.color)) {
                    this.possibleMoves[x + 1][y + 1] = 1;
                }
                x++;
                y++;
            } else if (figures[x + 1][y + 1].color != this.color) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x + 1][y + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x + 1, y + 1, figures,
                        this.color)) {
                    this.possibleMoves[x + 1][y + 1] = 1;
                }
                break;
            } else {
                break;
            }

        }
        x = this.getPositionX();
        y = this.getPositionY();
        while (x - 1 >= 0 && y - 1 >= 0) {
            if (figures[x - 1][y - 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x - 1][y - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x - 1, y - 1, figures,
                        this.color)) {
                    this.possibleMoves[x - 1][y - 1] = 1;
                }
                x--;
                y--;
            } else if (figures[x - 1][y - 1].color != this.color) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x - 1][y - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x - 1, y - 1, figures, this.color)) {
                    this.possibleMoves[x - 1][y - 1] = 1;
                }
                break;
            } else {
                break;
            }
        }
        x = this.getPositionX();
        y = this.getPositionY();
        while (x + 1 < 8 && y - 1 >= 0) {
            if (figures[x + 1][y - 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x + 1][y - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x + 1, y - 1, figures,
                        this.color)) {
                    this.possibleMoves[x + 1][y - 1] = 1;
                }
                x++;
                y--;
            } else if (figures[x + 1][y - 1].color != this.color) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x + 1][y - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x + 1, y - 1, figures,
                        this.color)) {
                    this.possibleMoves[x + 1][y - 1] = 1;
                }
                break;
            } else {
                break;
            }
        }
        x = this.getPositionX();
        y = this.getPositionY();
        while (x - 1 >= 0 && y + 1 < 8) {
            if (figures[x - 1][y + 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x - 1][y + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x - 1, y + 1, figures,
                        this.color)) {
                    this.possibleMoves[x - 1][y + 1] = 1;
                }
                x--;
                y++;
            } else if (figures[x - 1][y + 1].color != this.color) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x - 1][y + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x - 1, y + 1, figures,
                        this.color)) {
                    this.possibleMoves[x - 1][y + 1] = 1;
                }
                break;
            } else {
                break;
            }
        }
        // Sets possible moves for a figure with diagonal movement behavior (e.g., Bishop and Queen).
        
        // @param alsoWithCasteling: A flag indicating whether to include castling moves in the possible moves.
        // @param figures: The 2D array representing the current state of the chessboard.
        // @param mustCheckCheck: A flag indicating whether to check for potential checks during the moves.
    }

    /**
     * Sets possible moves for a figure with straight-line movement behavior (e.g., Rook and Queen).
     *
     * @param alsoWithCasteling A flag indicating whether to include castling moves in the possible moves.
     * @param figures The 2D array representing the current state of the chessboard.
     * @param mustCheckCheck A flag indicating whether to check for potential checks during the moves.
     */
    public void rookLikeSetPossibleMoves(boolean alsoWithCasteling, Figure[][] figures,
            boolean mustCheckCheck) {
        int x = this.getPositionX();
        int y = this.getPositionY();
        while (x + 1 < 8) {
            if (figures[x + 1][y] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x + 1][y] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x + 1, y, figures,
                        this.color)) {
                    this.possibleMoves[x + 1][y] = 1;
                }
                x++;
            } else if (figures[x + 1][y].color != this.color) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x + 1][y] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x + 1, y, figures,
                        this.color)) {
                    this.possibleMoves[x + 1][y] = 1;
                }
                break;
            } else {
                break;
            }
        }
        x = this.getPositionX();
        y = this.getPositionY();
        while (x - 1 >= 0) {
            if (figures[x - 1][y] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x - 1][y] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x - 1, y, figures,
                        this.color)) {
                    this.possibleMoves[x - 1][y] = 1;
                }
                x--;
            } else if (figures[x - 1][y].color != this.color) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x - 1][y] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x - 1, y, figures,
                        this.color)) {
                    this.possibleMoves[x - 1][y] = 1;
                }
                break;
            } else {
                break;
            }
        }
        x = this.getPositionX();
        y = this.getPositionY();
        while (y + 1 < 8) {
            if (figures[x][y + 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x][y + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x, y + 1, figures,
                        this.color)) {
                    this.possibleMoves[x][y + 1] = 1;
                }
                y++;
            } else if (figures[x][y + 1].color != this.color) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x][y + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x, y + 1, figures,
                        this.color)) {
                    this.possibleMoves[x][y + 1] = 1;
                }
                break;
            } else {
                break;
            }
        }
        x = this.getPositionX();
        y = this.getPositionY();
        while (y - 1 >= 0) {
            if (figures[x][y - 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x][y - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x, y - 1, figures,
                        this.color)) {
                    this.possibleMoves[x][y - 1] = 1;
                }
                y--;
            } else if (figures[x][y - 1].color != this.color) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[x][y - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), x, y - 1, figures,
                        this.color)) {
                    this.possibleMoves[x][y - 1] = 1;
                }
                break;
            } else {
                break;
            }
        }
        // Sets possible moves for a figure with straight-line movement behavior (e.g., Rook and Queen).

        // @param alsoWithCasteling: A flag indicating whether to include castling moves in the possible moves.
        // @param figures: The 2D array representing the current state of the chessboard.
        // @param mustCheckCheck: A flag indicating whether to check for potential checks during the moves.
    }

    /**
     * Sets the possible moves for a pawn, considering special moves like double moves
     * on its first move and capturing en passant.
     *
     * @param alsoWithCasteling A flag indicating whether to include castling moves (not used for pawns).
     * @param figures The current state of the chessboard.
     * @param mustCheckCheck A flag indicating whether to check if the move puts the own king in check.
     */
    public void pawnSetPossibleMoves(boolean alsoWithCasteling, Figure[][] figures, boolean mustCheckCheck) {
        if (this.color.equals("white")) {
            // White pawn special moves
            if (this.getPositionY() == 4) {
                // Check en passant capture to the right
                if (this.getPositionX() + 1 < 8 && this.getPositionX() + 1 == lastMoveOfPawn[0]
                        && this.getPositionY() == lastMoveOfPawn[1]) {
                    if (mustCheckCheck == false) {
                        this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 1] = 1;
                    } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                            this.getPositionY(), this.getPositionX() + 1, this.getPositionY() + 1, figures,
                            this.color)) {
                        this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 1] = 1;
                    }
                }
                // Check en passant capture to the left
                if (this.getPositionX() - 1 >= 0 && this.getPositionX() - 1 == lastMoveOfPawn[0]
                        && this.getPositionY() == lastMoveOfPawn[1]) {
                    if (mustCheckCheck == false) {
                        this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 1] = 1;
                    } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                            this.getPositionY(), this.getPositionX() - 1, this.getPositionY() + 1, figures,
                            this.color)) {
                        this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 1] = 1;
                    }
                }
            }
            // Check double move on the first move
            if (this.getPositionY() == 1 && figures[this.getPositionX()][this.getPositionY() + 2] == null
                    && figures[this.getPositionX()][this.getPositionY() + 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[this.getPositionX()][this.getPositionY() + 2] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), this.getPositionX(), this.getPositionY() + 2, figures,
                        this.color)) {
                    this.possibleMoves[this.getPositionX()][this.getPositionY() + 2] = 1;
                }
            }
            // Check capturing moves to the right and left
            if (this.getPositionX() + 1 < 8 && this.getPositionY() + 1 < 8
                    && figures[this.getPositionX() + 1][this.getPositionY() + 1] != null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), this.getPositionX() + 1, this.getPositionY() + 1, figures,
                        this.color)) {
                    this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 1] = 1;
                }
            }
            if (this.getPositionX() - 1 >= 0 && this.getPositionY() + 1 < 8
                    && figures[this.getPositionX() - 1][this.getPositionY() + 1] != null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), this.getPositionX() - 1, this.getPositionY() + 1, figures,
                        this.color)) {
                    this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 1] = 1;
                }
            }
            // Check simple move forward
            if (this.getPositionY() + 1 < 8
                    && figures[this.getPositionX()][this.getPositionY() + 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[this.getPositionX()][this.getPositionY() + 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), this.getPositionX(), this.getPositionY() + 1, figures,
                        this.color)) {
                    this.possibleMoves[this.getPositionX()][this.getPositionY() + 1] = 1;
                }
            }
        } else {
            // Black pawn special moves
            if (this.getPositionY() == 3) {
                // Check en passant capture to the right
                if (this.getPositionX() + 1 < 8 && this.getPositionX() + 1 == lastMoveOfPawn[0]
                        && this.getPositionY() == lastMoveOfPawn[1]) {
                    if (mustCheckCheck == false) {
                        this.possibleMoves[this.getPositionX() + 1][this.getPositionY() + 1] = 1;
                    } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                            this.getPositionY(), this.getPositionX() + 1, this.getPositionY() - 1, figures,
                            this.color)) {
                        this.possibleMoves[this.getPositionX() + 1][this.getPositionY() - 1] = 1;
                    }
                }
                // Check en passant capture to the left
                if (this.getPositionX() - 1 >= 0 && this.getPositionX() - 1 == lastMoveOfPawn[0]
                        && this.getPositionY() == lastMoveOfPawn[1]) {
                    if (mustCheckCheck == false) {
                        this.possibleMoves[this.getPositionX() - 1][this.getPositionY() + 1] = 1;
                    } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                            this.getPositionY(), this.getPositionX() - 1, this.getPositionY() - 1, figures,
                            this.color)) {
                        this.possibleMoves[this.getPositionX() - 1][this.getPositionY() - 1] = 1;
                    }
                }
            }
            // Check double move on the first move
            if (this.getPositionY() == 6 && figures[this.getPositionX()][this.getPositionY() - 2] == null
                    && figures[this.getPositionX()][this.getPositionY() - 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[this.getPositionX()][this.getPositionY() - 2] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), this.getPositionX(), this.getPositionY() - 2, figures,
                        this.color)) {
                    this.possibleMoves[this.getPositionX()][this.getPositionY() - 2] = 1;
                }
            }
            // Check capturing moves to the right and left
            if (this.getPositionX() + 1 < 8 && this.getPositionY() - 1 >= 0
                    && figures[this.getPositionX() + 1][this.getPositionY() - 1] != null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[this.getPositionX() + 1][this.getPositionY() - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), this.getPositionX() + 1, this.getPositionY() - 1, figures,
                        this.color)) {
                    this.possibleMoves[this.getPositionX() + 1][this.getPositionY() - 1] = 1;
                }
            }
            if (this.getPositionX() - 1 >= 0 && this.getPositionY() - 1 >= 0
                    && figures[this.getPositionX() - 1][this.getPositionY() - 1] != null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[this.getPositionX() - 1][this.getPositionY() - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), this.getPositionX() - 1, this.getPositionY() - 1, figures,
                        this.color)) {
                    this.possibleMoves[this.getPositionX() - 1][this.getPositionY() - 1] = 1;
                }
            }
            // Check simple move forward
            if (this.getPositionY() - 1 >= 0
                    && figures[this.getPositionX()][this.getPositionY() - 1] == null) {
                if (mustCheckCheck == false) {
                    this.possibleMoves[this.getPositionX()][this.getPositionY() - 1] = 1;
                } else if (!checkIfAfterMoveWillBeInCheck(this.getPositionX(),
                        this.getPositionY(), this.getPositionX(), this.getPositionY() - 1, figures,
                        this.color)) {
                    this.possibleMoves[this.getPositionX()][this.getPositionY() - 1] = 1;
                }
            }
        }
    }


    /**
     * Sets the possible moves for the current chess piece on the board.
     *
     * @param alsoWithCasteling A flag indicating whether to include castling moves.
     * @param figures The current state of the chessboard.
     * @param mustCheckCheck A flag indicating whether to check if the move puts the own king in check.
     */
    public void setPossibleMoves(boolean alsoWithCasteling, Figure[][] figures, boolean mustCheckCheck) {

        if (this.possibleMovesIsSet == true) {
            return;
        }

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this.possibleMoves[i][j] = 0;
            }
        }
        if (this instanceof King) {
            if (alsoWithCasteling) {
                castelingSetPossibleMoves(alsoWithCasteling, figures, mustCheckCheck);
            }
            kingRestSetPossibleMoves(alsoWithCasteling, figures, mustCheckCheck);
        }
        if (this instanceof Knight) {
            knightSetPossibleMoves(alsoWithCasteling, figures, mustCheckCheck);
        }
        if (this instanceof Bishop) {
            bishopLikeSetPossibleMoves(alsoWithCasteling, figures, mustCheckCheck);
        }
        if (this instanceof Rook) {
            rookLikeSetPossibleMoves(alsoWithCasteling, figures, mustCheckCheck);
        }
        if (this instanceof Queen) {
            bishopLikeSetPossibleMoves(alsoWithCasteling, figures, mustCheckCheck);
            rookLikeSetPossibleMoves(alsoWithCasteling, figures, mustCheckCheck);
        }
        if (this instanceof Pawn) {
            pawnSetPossibleMoves(alsoWithCasteling, figures, mustCheckCheck);
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (figures[i][j] != null) {
                    if (figures[i][j].color == this.color) {
                        if (this.possibleMoves[i][j] == 1) {
                            this.possibleMoves[i][j] = 0;
                        }
                    }
                }
            }
        }
    }

    /**
     * Updates the position of the chess piece on the chessboard.
     *
     * @param newPositionX The new X-coordinate of the chess piece.
     * @param newPositionY The new Y-coordinate of the chess piece.
     * @param wasTherePromotion A string indicating if there was a promotion during the move.
     */
    public void setPosition(int newPositionX, int newPositionY, String wasTherePromotion) {
        Main.fiftyMoveCounter++;
        if (this instanceof King || this instanceof Rook) {
            this.canCastle = false;
        }
        if (this instanceof King) {
            if (this.color == "white" && this.positionX - newPositionX == 2) {
                Figure rookPointer = Main.figures[0][0];
                rookPointer.canCastle = false;
                rookPointer.positionX = 3;
                rookPointer.positionY = 0;
                Main.figures[0][0] = null;
                Main.figures[3][0] = rookPointer;
                Main.positionCountMapWhite = new HashMap<>();
                Main.positionCountMapBlack = new HashMap<>();
            }
            if (this.color == "white" && this.positionX - newPositionX == -2) {
                Figure rookPointer = Main.figures[7][0];
                rookPointer.canCastle = false;
                rookPointer.positionX = 5;
                rookPointer.positionY = 0;
                Main.figures[7][0] = null;
                Main.figures[5][0] = rookPointer;
                Main.positionCountMapWhite = new HashMap<>();
                Main.positionCountMapBlack = new HashMap<>();
            }
            if (this.color == "black" && this.positionX - newPositionX == 2) {
                Figure rookPointer = Main.figures[0][7];
                rookPointer.canCastle = false;
                rookPointer.positionX = 3;
                rookPointer.positionY = 7;
                Main.figures[0][7] = null;
                Main.figures[3][7] = rookPointer;
                Main.positionCountMapWhite = new HashMap<>();
                Main.positionCountMapBlack = new HashMap<>();
            }
            if (this.color == "black" && this.positionX - newPositionX == -2) {
                Figure rookPointer = Main.figures[7][7];
                rookPointer.canCastle = false;
                rookPointer.positionX = 5;
                rookPointer.positionY = 7;
                Main.figures[7][7] = null;
                Main.figures[5][7] = rookPointer;
                Main.positionCountMapWhite = new HashMap<>();
                Main.positionCountMapBlack = new HashMap<>();
            }
        }
        if (this instanceof Pawn) {

            Main.fiftyMoveCounter = 0;
            Main.positionCountMapWhite = new HashMap<>();
            Main.positionCountMapBlack = new HashMap<>();

            if (newPositionY == 0 || newPositionY == 7) {
                if (wasTherePromotion == "x") {
                    whatToAdd(newPositionX, newPositionY);
                } else {
                    switch (wasTherePromotion) {
                        case "xqueen":
                            promoted = "queen";
                            Main.figureList.add(new Queen(newPositionX, newPositionY, color));
                            break;
                        case "xrook":
                            promoted = "rook";
                            Main.figureList.add(new Rook(newPositionX, newPositionY, color));
                            break;
                        case "xknight":
                            promoted = "knight";
                            Main.figureList.add(new Knight(newPositionX, newPositionY, color));
                            break;
                        case "xbishop":
                            promoted = "bishop";
                            if (newPositionX + newPositionY % 2 == 0) {
                                Main.figureList.add(new Bishop(newPositionX, newPositionY, color, "black"));
                            } else {
                                Main.figureList.add(new Bishop(newPositionX, newPositionY, color, "white"));
                            }

                            break;
                    }

                    Main.figures[positionX][positionY] = null;
                    Main.figures[newPositionX][newPositionY] = Main.figureList.get(Main.figureList.size() - 1);
                    Main.figureList.remove(this);

                }
                return;
            }

            if (this.getPositionX() - newPositionX != 0 && Main.figures[newPositionX][newPositionY] == null) {
                if (this.color == "white") {
                    Main.figureList.remove(Main.figures[newPositionX][newPositionY - 1]);
                    Main.figures[newPositionX][newPositionY - 1] = null;
                } else {
                    Main.figureList.remove(Main.figures[newPositionX][newPositionY + 1]);
                    Main.figures[newPositionX][newPositionY + 1] = null;
                }
            }

            if (this.getPositionY() - newPositionY == 2 || this.getPositionY() - newPositionY == -2) {
                lastMoveOfPawn[0] = newPositionX;
                lastMoveOfPawn[1] = newPositionY;
            } else {
                if (lastMoveOfPawn[0] != -1) {
                    Main.positionCountMapWhite = new HashMap<>();
                    Main.positionCountMapBlack = new HashMap<>();
                }
                lastMoveOfPawn[0] = -1;
                lastMoveOfPawn[1] = -1;
            }
        } else {
            if (lastMoveOfPawn[0] != -1) {
                Main.positionCountMapWhite = new HashMap<>();
                Main.positionCountMapBlack = new HashMap<>();
            }
            lastMoveOfPawn[0] = -1;
            lastMoveOfPawn[1] = -1;
        }

        Main.figures[this.positionX][this.positionY] = null;
        Main.figures[newPositionX][newPositionY] = this;
        this.positionX = newPositionX;
        this.positionY = newPositionY;
    }

    /**
     * Displays a dialog prompting the user to choose a piece for pawn promotion.
     * Handles the promotion logic based on the user's choice.
     *
     * @param newPositionX The new X-coordinate of the promoted piece.
     * @param newPositionY The new Y-coordinate of the promoted piece.
     */
    public void whatToAdd(int newPositionX, int newPositionY) {
        JFrame frame = new JFrame("");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JDialog dialog = new JDialog(frame, "Choose Your Piece", true);
        dialog.setSize(800, 800);
        dialog.setLayout(new GridLayout(2, 2));

        ImageIcon image1;
        ImageIcon image2;
        ImageIcon image3;
        ImageIcon image4;

        if (color.equals("white")) {
            image1 = new ImageIcon(PATH_IMAGE + "queen_white.png");
            image2 = new ImageIcon(PATH_IMAGE + "rook_white.png");
            image3 = new ImageIcon(PATH_IMAGE + "knight_white.png");
            image4 = new ImageIcon(PATH_IMAGE + "bishop_white.png");
        } else {
            image1 = new ImageIcon(PATH_IMAGE + "queen_black.png");
            image2 = new ImageIcon(PATH_IMAGE + "rook_black.png");
            image3 = new ImageIcon(PATH_IMAGE + "knight_black.png");
            image4 = new ImageIcon(PATH_IMAGE + "bishop_black.png");
        }

        JButton button1 = new JButton(image1);
        JButton button2 = new JButton(image2);
        JButton button3 = new JButton(image3);
        JButton button4 = new JButton(image4);

        String[] choice = { "" };

        button1.addActionListener((ActionListener) new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choice[0] = "queen";
                dialog.dispose();
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choice[0] = "rook";
                dialog.dispose();
            }
        });

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choice[0] = "knight";
                dialog.dispose();
            }
        });

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choice[0] = "bishop";
                dialog.dispose();
            }
        });

        dialog.add(button1);
        dialog.add(button2);
        dialog.add(button3);
        dialog.add(button4);

        dialog.setLocationRelativeTo(frame);
        dialog.setVisible(true);

        switch (choice[0]) {
            case "queen":
                promoted = "queen";
                Main.figureList.add(new Queen(newPositionX, newPositionY, color));
                break;
            case "rook":
                promoted = "rook";
                Main.figureList.add(new Rook(newPositionX, newPositionY, color));
                break;
            case "knight":
                promoted = "knight";
                Main.figureList.add(new Knight(newPositionX, newPositionY, color));
                break;
            case "bishop":
                promoted = "bishop";
                if (newPositionX + newPositionY % 2 == 0) {
                    Main.figureList.add(new Bishop(newPositionX, newPositionY, color, "black"));
                } else {
                    Main.figureList.add(new Bishop(newPositionX, newPositionY, color, "white"));
                }

                break;
        }

        Main.figures[positionX][positionY] = null;
        Main.figures[newPositionX][newPositionY] = Main.figureList.get(Main.figureList.size() - 1);
        Main.figureList.remove(this);

        frame.dispose();
    }

    /**
     * Validates whether the specified move is within the chessboard boundaries and allowed for the current piece.
     *
     * @param newPositionX The X-coordinate of the destination position.
     * @param newPositionY The Y-coordinate of the destination position.
     * @return True if the move is valid, false otherwise.
     */
    public boolean validateMove(int newPositionX, int newPositionY) {
        // Check if the new position is within the chessboard boundaries
        if (newPositionX >= 0 && newPositionX < 8 && newPositionY >= 0 && newPositionY < 8) {
            // Check if the move is allowed for the current piece
            if (this.possibleMoves[newPositionX][newPositionY] == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the opponent has a piece that can attack the specified square.
     *
     * @param x         The X-coordinate of the square to be checked.
     * @param y         The Y-coordinate of the square to be checked.
     * @param figures   The 2D array representing the chessboard with chess pieces.
     * @param whoPlays  The color of the player whose turn it is ("white" or "black").
     * @return True if an opponent's piece can attack the specified square, false otherwise.
     */
    public static boolean checkIfOpponentSeesThisSquare(int x, int y, Figure[][] figures, String whoPlays) {

        if (whoPlays == "white") {
            if (x + 1 >= 0 && x + 1 < 8 && y + 1 >= 0 && y + 1 < 8 && figures[x + 1][y + 1] != null
                    && figures[x + 1][y + 1] instanceof Pawn && figures[x + 1][y + 1].color == "black") {
                return true;
            }
            if (x - 1 >= 0 && x - 1 < 8 && y + 1 >= 0 && y + 1 < 8 && figures[x - 1][y + 1] != null
                    && figures[x - 1][y + 1] instanceof Pawn && figures[x - 1][y + 1].color == "black") {
                return true;
            }
            if (x != 0) {
                for (int i = x - 1; i >= 0; i--) {
                    if (figures[i][y] != null) {
                        if (figures[i][y].color == "white") {
                            break;
                        } else {
                            if (figures[i][y] instanceof Queen || figures[i][y] instanceof Rook) {
                                return true;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (x != 7) {
                for (int i = x + 1; i < 8; i++) {
                    if (figures[i][y] != null) {
                        if (figures[i][y].color == "white") {
                            break;
                        } else {
                            if (figures[i][y] instanceof Queen || figures[i][y] instanceof Rook) {
                                return true;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (y != 0) {
                for (int i = y - 1; i >= 0; i--) {
                    if (figures[x][i] != null) {
                        if (figures[x][i].color == "white") {
                            break;
                        } else {
                            if (figures[x][i] instanceof Queen || figures[x][i] instanceof Rook) {
                                return true;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (y != 7) {
                for (int i = y + 1; i < 8; i++) {
                    if (figures[x][i] != null) {
                        if (figures[x][i].color == "white") {
                            break;
                        } else {
                            if (figures[x][i] instanceof Queen || figures[x][i] instanceof Rook) {
                                return true;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }

            if (x != 0 && y != 0) {
                int j = y - 1;
                outerLoop: for (int i = x - 1; i >= 0; i--) {
                    if (j < 0) {
                        break outerLoop;
                    }
                    if (figures[i][j] != null) {
                        if (figures[i][j].color == "white") {
                            break outerLoop;
                        } else {
                            if (figures[i][j] instanceof Queen || figures[i][j] instanceof Bishop) {
                                return true;
                            } else {
                                break outerLoop;
                            }
                        }
                    }
                    j--;
                }
            }

            if (x != 0 && y != 7) {
                int j = y + 1;
                outerLoop: for (int i = x - 1; i >= 0; i--) {
                    if (j >= 8) {
                        break outerLoop;
                    }
                    if (figures[i][j] != null) {
                        if (figures[i][j].color == "white") {
                            break outerLoop;
                        } else {
                            if (figures[i][j] instanceof Queen || figures[i][j] instanceof Bishop) {
                                return true;
                            } else {
                                break outerLoop;
                            }
                        }
                    }
                    j++;
                }
            }

            if (x != 7 && y != 7) {
                int j = y + 1;
                outerLoop: for (int i = x + 1; i < 8; i++) {
                    if (j >= 8) {
                        break outerLoop;
                    }
                    if (figures[i][j] != null) {
                        if (figures[i][j].color == "white") {
                            break outerLoop;
                        } else {
                            if (figures[i][j] instanceof Queen || figures[i][j] instanceof Bishop) {
                                return true;
                            } else {
                                break outerLoop;
                            }
                        }
                    }
                    j++;
                }
            }

            if (x != 7 && y != 0) {
                int j = y - 1;
                outerLoop: for (int i = x + 1; i < 8; i++) {
                    if (j < 0) {
                        break outerLoop;
                    }
                    if (figures[i][j] != null) {
                        if (figures[i][j].color == "white") {
                            break outerLoop;
                        } else {
                            if (figures[i][j] instanceof Queen || figures[i][j] instanceof Bishop) {
                                return true;
                            } else {
                                break outerLoop;
                            }
                        }
                    }
                    j--;

                }
            }

            if (x + 2 < 8 && y + 1 < 8) {
                if (figures[x + 2][y + 1] != null && figures[x + 2][y + 1] instanceof Knight
                        && figures[x + 2][y + 1].color == "black") {
                    return true;
                }
            }
            if (x + 1 < 8 && y + 2 < 8) {
                if (figures[x + 1][y + 2] != null && figures[x + 1][y + 2] instanceof Knight
                        && figures[x + 1][y + 2].color == "black") {
                    return true;
                }
            }
            if (x - 2 >= 0 && y + 1 < 8) {
                if (figures[x - 2][y + 1] != null && figures[x - 2][y + 1] instanceof Knight
                        && figures[x - 2][y + 1].color == "black") {
                    return true;
                }
            }
            if (x - 1 >= 0 && y + 2 < 8) {
                if (figures[x - 1][y + 2] != null && figures[x - 1][y + 2] instanceof Knight
                        && figures[x - 1][y + 2].color == "black") {
                    return true;
                }
            }

            if (x + 2 < 8 && y - 1 >= 0) {
                if (figures[x + 2][y - 1] != null && figures[x + 2][y - 1] instanceof Knight
                        && figures[x + 2][y - 1].color == "black") {
                    return true;
                }
            }
            if (x + 1 < 8 && y - 2 >= 0) {
                if (figures[x + 1][y - 2] != null && figures[x + 1][y - 2] instanceof Knight
                        && figures[x + 1][y - 2].color == "black") {
                    return true;
                }
            }
            if (x - 2 >= 0 && y - 1 >= 0) {
                if (figures[x - 2][y - 1] != null && figures[x - 2][y - 1] instanceof Knight
                        && figures[x - 2][y - 1].color == "black") {
                    return true;
                }
            }
            if (x - 1 >= 0 && y - 2 >= 0) {
                if (figures[x - 1][y - 2] != null && figures[x - 1][y - 2] instanceof Knight
                        && figures[x - 1][y - 2].color == "black") {
                    return true;
                }
            }
            if (x - 1 >= 0 && y - 1 >= 0) {
                if (figures[x - 1][y - 1] != null && figures[x - 1][y - 1] instanceof King
                        && figures[x - 1][y - 1].color == "black") {
                    return true;
                }
            }
            if (x + 1 < 8 && y - 1 >= 0) {
                if (figures[x + 1][y - 1] != null && figures[x + 1][y - 1] instanceof King
                        && figures[x + 1][y - 1].color == "black") {
                    return true;
                }
            }
            if (x - 1 >= 0 && y + 1 < 8) {
                if (figures[x - 1][y + 1] != null && figures[x - 1][y + 1] instanceof King
                        && figures[x - 1][y + 1].color == "black") {
                    return true;
                }
            }
            if (x + 1 < 8 && y + 1 < 8) {
                if (figures[x + 1][y + 1] != null && figures[x + 1][y + 1] instanceof King
                        && figures[x + 1][y + 1].color == "black") {
                    return true;
                }
            }
            if (x - 1 >= 0) {
                if (figures[x - 1][y] != null && figures[x - 1][y] instanceof King
                        && figures[x - 1][y].color == "black") {
                    return true;
                }
            }
            if (x + 1 < 8) {
                if (figures[x + 1][y] != null && figures[x + 1][y] instanceof King
                        && figures[x + 1][y].color == "black") {
                    return true;
                }
            }
            if (y - 1 >= 0) {
                if (figures[x][y - 1] != null && figures[x][y - 1] instanceof King
                        && figures[x][y - 1].color == "black") {
                    return true;
                }
            }
            if (y + 1 < 8) {
                if (figures[x][y + 1] != null && figures[x][y + 1] instanceof King
                        && figures[x][y + 1].color == "black") {
                    return true;
                }
            }

        } else {
            if (x + 1 >= 0 && x + 1 < 8 && y - 1 >= 0 && y - 1 < 8 && figures[x + 1][y - 1] != null
                    && figures[x + 1][y - 1] instanceof Pawn && figures[x + 1][y - 1].color == "white") {
                return true;
            }
            if (x - 1 >= 0 && x - 1 < 8 && y - 1 >= 0 && y - 1 < 8 && figures[x - 1][y - 1] != null
                    && figures[x - 1][y - 1] instanceof Pawn && figures[x - 1][y - 1].color == "white") {
                return true;
            }
            if (x != 0) {
                for (int i = x - 1; i >= 0; i--) {
                    if (figures[i][y] != null) {
                        if (figures[i][y].color == "black") {
                            break;
                        } else {
                            if (figures[i][y] instanceof Queen || figures[i][y] instanceof Rook) {
                                return true;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (x != 7) {
                for (int i = x + 1; i < 8; i++) {
                    if (figures[i][y] != null) {
                        if (figures[i][y].color == "black") {
                            break;
                        } else {
                            if (figures[i][y] instanceof Queen || figures[i][y] instanceof Rook) {
                                return true;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (y != 0) {
                for (int i = y - 1; i >= 0; i--) {
                    if (figures[x][i] != null) {
                        if (figures[x][i].color == "black") {
                            break;
                        } else {
                            if (figures[x][i] instanceof Queen || figures[x][i] instanceof Rook) {
                                return true;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (y != 7) {
                for (int i = y + 1; i < 8; i++) {
                    if (figures[x][i] != null) {
                        if (figures[x][i].color == "black") {
                            break;
                        } else {
                            if (figures[x][i] instanceof Queen || figures[x][i] instanceof Rook) {
                                return true;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }

            if (x != 0 && y != 0) {
                int j = y - 1;
                outerLoop: for (int i = x - 1; i >= 0; i--) {
                    if (j < 0) {
                        break outerLoop;
                    }
                    if (figures[i][j] != null) {
                        if (figures[i][j].color == "black") {
                            break outerLoop;
                        } else {
                            if (figures[i][j] instanceof Queen || figures[i][j] instanceof Bishop) {
                                return true;
                            } else {
                                break outerLoop;
                            }
                        }
                    }
                    j--;
                }
            }

            if (x != 0 && y != 7) {
                int j = y + 1;
                outerLoop: for (int i = x - 1; i >= 0; i--) {
                    if (j >= 8) {
                        break outerLoop;
                    }
                    if (figures[i][j] != null) {
                        if (figures[i][j].color == "black") {
                            break outerLoop;
                        } else {
                            if (figures[i][j] instanceof Queen || figures[i][j] instanceof Bishop) {
                                return true;
                            } else {
                                break outerLoop;
                            }
                        }
                    }
                    j++;
                }
            }

            if (x != 7 && y != 7) {
                int j = y + 1;
                outerLoop: for (int i = x + 1; i < 8; i++) {
                    if (j >= 8) {
                        break outerLoop;
                    }
                    if (figures[i][j] != null) {
                        if (figures[i][j].color == "black") {
                            break outerLoop;
                        } else {
                            if (figures[i][j] instanceof Queen || figures[i][j] instanceof Bishop) {
                                return true;
                            } else {
                                break outerLoop;
                            }
                        }
                    }
                    j++;
                }
            }

            if (x != 7 && y != 0) {
                int j = y - 1;
                outerLoop: for (int i = x + 1; i < 8; i++) {
                    if (j < 0) {
                        break outerLoop;
                    }
                    if (figures[i][j] != null) {
                        if (figures[i][j].color == "black") {
                            break outerLoop;
                        } else {
                            if (figures[i][j] instanceof Queen || figures[i][j] instanceof Bishop) {
                                return true;
                            } else {
                                break outerLoop;
                            }
                        }
                    }
                    j--;

                }
            }

            if (x + 2 < 8 && y + 1 < 8) {
                if (figures[x + 2][y + 1] != null && figures[x + 2][y + 1] instanceof Knight
                        && figures[x + 2][y + 1].color == "white") {
                    return true;
                }
            }
            if (x + 1 < 8 && y + 2 < 8) {
                if (figures[x + 1][y + 2] != null && figures[x + 1][y + 2] instanceof Knight
                        && figures[x + 1][y + 2].color == "white") {
                    return true;
                }
            }
            if (x - 2 >= 0 && y + 1 < 8) {
                if (figures[x - 2][y + 1] != null && figures[x - 2][y + 1] instanceof Knight
                        && figures[x - 2][y + 1].color == "white") {
                    return true;
                }
            }
            if (x - 1 >= 0 && y + 2 < 8) {
                if (figures[x - 1][y + 2] != null && figures[x - 1][y + 2] instanceof Knight
                        && figures[x - 1][y + 2].color == "white") {
                    return true;
                }
            }

            if (x + 2 < 8 && y - 1 >= 0) {
                if (figures[x + 2][y - 1] != null && figures[x + 2][y - 1] instanceof Knight
                        && figures[x + 2][y - 1].color == "white") {
                    return true;
                }
            }
            if (x + 1 < 8 && y - 2 >= 0) {
                if (figures[x + 1][y - 2] != null && figures[x + 1][y - 2] instanceof Knight
                        && figures[x + 1][y - 2].color == "white") {
                    return true;
                }
            }
            if (x - 2 >= 0 && y - 1 >= 0) {
                if (figures[x - 2][y - 1] != null && figures[x - 2][y - 1] instanceof Knight
                        && figures[x - 2][y - 1].color == "white") {
                    return true;
                }
            }
            if (x - 1 >= 0 && y - 2 >= 0) {
                if (figures[x - 1][y - 2] != null && figures[x - 1][y - 2] instanceof Knight
                        && figures[x - 1][y - 2].color == "white") {
                    return true;
                }
            }
            if (x - 1 >= 0 && y - 1 >= 0) {
                if (figures[x - 1][y - 1] != null && figures[x - 1][y - 1] instanceof King
                        && figures[x - 1][y - 1].color == "white") {
                    return true;
                }
            }
            if (x + 1 < 8 && y - 1 >= 0) {
                if (figures[x + 1][y - 1] != null && figures[x + 1][y - 1] instanceof King
                        && figures[x + 1][y - 1].color == "white") {
                    return true;
                }
            }
            if (x - 1 >= 0 && y + 1 < 8) {
                if (figures[x - 1][y + 1] != null && figures[x - 1][y + 1] instanceof King
                        && figures[x - 1][y + 1].color == "white") {
                    return true;
                }
            }
            if (x + 1 < 8 && y + 1 < 8) {
                if (figures[x + 1][y + 1] != null && figures[x + 1][y + 1] instanceof King
                        && figures[x + 1][y + 1].color == "white") {
                    return true;
                }
            }
            if (x - 1 >= 0) {
                if (figures[x - 1][y] != null && figures[x - 1][y] instanceof King
                        && figures[x - 1][y].color == "white") {
                    return true;
                }
            }
            if (x + 1 < 8) {
                if (figures[x + 1][y] != null && figures[x + 1][y] instanceof King
                        && figures[x + 1][y].color == "white") {
                    return true;
                }
            }
            if (y - 1 >= 0) {
                if (figures[x][y - 1] != null && figures[x][y - 1] instanceof King
                        && figures[x][y - 1].color == "white") {
                    return true;
                }
            }
            if (y + 1 < 8) {
                if (figures[x][y + 1] != null && figures[x][y + 1] instanceof King
                        && figures[x][y + 1].color == "white") {
                    return true;
                }
            }

        }

        return false;
    }

    /**
     * Checks if making a move from the specified source square to the destination square
     * will result in the player being in check.
     *
     * @param moveFromX The X-coordinate of the source square.
     * @param moveFromY The Y-coordinate of the source square.
     * @param moveToX   The X-coordinate of the destination square.
     * @param moveToY   The Y-coordinate of the destination square.
     * @param figures   The 2D array representing the chessboard with chess pieces.
     * @param color     The color of the player making the move ("white" or "black").
     * @return True if the move results in the player being in check, false otherwise.
     */
    public static boolean checkIfAfterMoveWillBeInCheck(int moveFromX, int moveFromY, int moveToX,
    int moveToY, Figure[][] figures, String color) {

    // Create a copy of the chessboard
    Figure[][] copiedFigures = new Figure[8][8];

    // Initialize a King object for the player's king
    King king = new King(moveToX, moveToY, color);

    // Copy the figures to the new array
    for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
        if (figures[i][j] != null) {
            copiedFigures[i][j] = (Figure) figures[i][j].clone();
            if (copiedFigures[i][j] instanceof King && copiedFigures[i][j].color.equals(color)) {
                king = (King) copiedFigures[i][j];
            }
        } else {
            copiedFigures[i][j] = null;
        }
    }
    }

    // Move the piece to the destination square in the copied array
    Figure movedFigure = copiedFigures[moveFromX][moveFromY];
    if (copiedFigures[moveFromX][moveFromY] instanceof Pawn && moveToX != moveFromX
        && copiedFigures[moveToX][moveToY] == null) {
    copiedFigures[moveToX][moveFromY] = null;
    }
    copiedFigures[moveFromX][moveFromY] = null;
    copiedFigures[moveToX][moveToY] = movedFigure;
    copiedFigures[moveToX][moveToY].positionX = moveToX;
    copiedFigures[moveToX][moveToY].positionY = moveToY;

    // Check if the player is in check after the move
    return king.isInCheck(copiedFigures);
    }

    /**
    * Gets the X-coordinate of the chess piece on the board.
    *
    * @return The X-coordinate of the chess piece.
    */
    public int getPositionX() {
    return this.positionX;
    }

    /**
    * Gets the Y-coordinate of the chess piece on the board.
    *
    * @return The Y-coordinate of the chess piece.
    */
    public int getPositionY() {
    return this.positionY;
    }

    /**
    * Gets the image of the chess piece.
    *
    * @return The BufferedImage representing the image of the chess piece.
    */
    public BufferedImage getFigureImage() {
    return figureImage;
    }

}