package cz.cuni.mff.benesada;

/**
 * Represents a Pawn chess piece.
 */
public class Pawn extends Figure {

    /**
     * Indicates whether the en passant move is possible for any Pawn.
     */
    public static boolean enPassant = false;

    /**
     * Constructor for the Pawn class.
     *
     * @param positionX The initial X-coordinate of the Pawn on the chessboard.
     * @param positionY The initial Y-coordinate of the Pawn on the chessboard.
     * @param color The color of the Pawn ("white" or "black").
     */
    public Pawn(int positionX, int positionY, String color) {
        super(positionX, positionY, color, "pawn");
        super.value = 1; // Setting the value of the Pawn piece.
    }

    /**
     * Moves the Pawn to a new position and prints the move details.
     *
     * @param newPositionX The new X-coordinate for the Pawn.
     * @param newPositionY The new Y-coordinate for the Pawn.
     * @param wasTherePromotion A string indicating if there was a pawn promotion during the move.
     * @return The result of the move operation.
     */
    @Override
    public String move(int newPositionX, int newPositionY, String wasTherePromotion) {
        System.out.println("Pawn moves to (" + newPositionX + ", " + newPositionY + ")");
        return super.move(newPositionX, newPositionY, wasTherePromotion);
    }
}
