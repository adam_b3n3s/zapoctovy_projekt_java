package cz.cuni.mff.benesada;

/**
 * Represents a Knight chess piece.
 */
public class Knight extends Figure {

    /**
     * Constructor for the Knight class.
     *
     * @param positionX The initial X-coordinate of the Knight on the chessboard.
     * @param positionY The initial Y-coordinate of the Knight on the chessboard.
     * @param color The color of the Knight ("white" or "black").
     */
    public Knight(int positionX, int positionY, String color) {
        super(positionX, positionY, color, "knight");
        super.value = 3; // Setting the value of the Knight piece.
    }

    /**
     * Moves the Knight to a new position and prints the move details.
     *
     * @param newPositionX The new X-coordinate for the Knight.
     * @param newPositionY The new Y-coordinate for the Knight.
     * @param wasTherePromotion A string indicating if there was a pawn promotion during the move.
     * @return The result of the move operation.
     */
    @Override
    public String move(int newPositionX, int newPositionY, String wasTherePromotion) {
        System.out.println("Knight moves to (" + newPositionX + ", " + newPositionY + ")");
        return super.move(newPositionX, newPositionY, wasTherePromotion);
    }
}
