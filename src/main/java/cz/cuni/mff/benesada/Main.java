package cz.cuni.mff.benesada;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.util.Random;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.awt.event.ActionEvent;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

/**
 * Main class.
 * 
 */
public class Main {

    public static Color DARK = Color.decode("#122403");
    public static Color LIGHT = Color.decode("#2A5207");

    static int code;

    public static int moveCounter = 0;

    public static int fiftyMoveCounter = 0;

    public static int row;
    public static int col;

    public static int[] lastMoveColoring = new int[4];

    public static BufferedImage bishopBlack;
    public static BufferedImage bishopWhite;
    public static BufferedImage kingBlack;
    public static BufferedImage kingWhite;
    public static BufferedImage knightBlack;
    public static BufferedImage knightWhite;
    public static BufferedImage pawnBlack;
    public static BufferedImage pawnWhite;
    public static BufferedImage queenBlack;
    public static BufferedImage queenWhite;
    public static BufferedImage rookBlack;
    public static BufferedImage rookWhite;

    public static String whoPlays = "white";
    static Timer timerWhite;
    static Timer timerBlack;
    public static int minutes = 1;
    public static int secondsPerMove;
    public static int secondsRemainingWhite;
    public static int secondsRemainingBlack;
    static boolean existsSelected = false;
    static boolean wantToChangeWiew = false;
    public static int gameType;

    public static String watchingPosition;
    public static String colorOfBot;

    static Object lock = new Object();

    public static Pawn aw = new Pawn(0, 1, "white");
    public static Pawn bw = new Pawn(1, 1, "white");
    public static Pawn cw = new Pawn(2, 1, "white");
    public static Pawn dw = new Pawn(3, 1, "white");
    public static Pawn ew = new Pawn(4, 1, "white");
    public static Pawn fw = new Pawn(5, 1, "white");
    public static Pawn gw = new Pawn(6, 1, "white");
    public static Pawn hw = new Pawn(7, 1, "white");

    public static Rook r2w = new Rook(0, 0, "white");
    public static Rook r1w = new Rook(7, 0, "white");
    public static Bishop b1w = new Bishop(5, 0, "white", "white");
    public static Bishop b2w = new Bishop(2, 0, "white", "black");
    public static Knight k1w = new Knight(6, 0, "white");
    public static Knight k2w = new Knight(1, 0, "white");
    public static King kw = new King(4, 0, "white");
    public static Queen qw = new Queen(3, 0, "white");

    public static Pawn ab = new Pawn(0, 6, "black");
    public static Pawn bb = new Pawn(1, 6, "black");
    public static Pawn cb = new Pawn(2, 6, "black");
    public static Pawn db = new Pawn(3, 6, "black");
    public static Pawn eb = new Pawn(4, 6, "black");
    public static Pawn fb = new Pawn(5, 6, "black");
    public static Pawn gb = new Pawn(6, 6, "black");
    public static Pawn hb = new Pawn(7, 6, "black");

    public static Rook r1b = new Rook(7, 7, "black");
    public static Rook r2b = new Rook(0, 7, "black");
    public static Bishop b1b = new Bishop(5, 7, "black", "black");
    public static Bishop b2b = new Bishop(2, 7, "black", "white");
    public static Knight k1b = new Knight(6, 7, "black");
    public static Knight k2b = new Knight(1, 7, "black");
    public static King kb = new King(4, 7, "black");
    public static Queen qb = new Queen(3, 7, "black");

    public static ArrayList<Figure> figureList = new ArrayList<>();

    public static Map<String, Integer> positionCountMapWhite = new HashMap<>();

    public static Map<String, Integer> positionCountMapBlack = new HashMap<>();

    public static Figure[][] figures = new Figure[8][8];

    public static Socket socket;

    static JPanel panel = new JPanel();

    private static JFrame frame;
    private static JFrame frameInput;
    private static JTextArea textArea;
    private static PrintStream stream;
    private static JTextField inputField;
    private static String userInput = "";
    private static Semaphore inputSemaphore = new Semaphore(0);

    private static JFrame frameEnd;

    /**
     * Sends a message to the server using the provided PrintWriter.
     *
     * @param msg    The message to be sent.
     * @param output PrintWriter for sending messages to the server.
     */
    static void sendMessage(String msg, PrintWriter output) {
        output.println(msg);
    }

    /**
     * Waits for a message from the server using the provided BufferedReader.
     *
     * @param input BufferedReader for receiving messages from the server.
     * @return The received message from the server.
     * @throws IOException If an I/O error occurs.
     */
    static String waitForMessage(BufferedReader input) throws IOException {
        String serverResponse = input.readLine();
        return serverResponse;
    }


    /**
     * Determines the color of the client (white or black) and handles time settings accordingly.
     *
     * @param output PrintWriter for sending messages to the server.
     * @param input BufferedReader for receiving messages from the server.
     * @throws IOException              If an I/O error occurs.
     * @throws NumberFormatException    If there is an error parsing a numeric value.
     * @throws InterruptedException     If the thread is interrupted.
     */
    static void clientColor(PrintWriter output, BufferedReader input)
            throws IOException, NumberFormatException, InterruptedException {
        if (Client.isClientTurn) {
            System.out.println("Client is white");
            watchingPosition = "white";

            System.out.println("How many minutes do you want: ");

            while (true) {
                try {
                    minutes = Integer.parseInt(waitForUserInput());
                    if (minutes > 0) {
                        break;
                    }
                } catch (NumberFormatException e) {

                }
            }

            System.out.println("");

            System.out.println("How many extra second per move do you want: ");

            while (true) {
                try {
                    secondsPerMove = Integer.parseInt(waitForUserInput());
                    if (secondsPerMove >= 0) {
                        break;
                    }
                } catch (NumberFormatException e) {

                }
            }

            System.out.println("");

            sendMessage(minutes + " " + secondsPerMove, output);
        } else {
            System.out.println("Client is black");
            watchingPosition = "black";
            String[] time = waitForMessage(input).split(" ");
            System.out.println("Minutes: " + time[0]);
            System.out.println("Seconds per move: " + time[1]);
            minutes = Integer.parseInt(time[0]);
            secondsPerMove = Integer.parseInt(time[1]);
        }
    }

    /**
     * Prints the starting information for the chess game and disposes of the input frame.
     */
    static void printStartingInfo() {
        System.out.println("");
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        System.out.println("");
        System.out.println("Game started!");
        frameInput.dispose();

    }

    /**
     * Initializes the chessboard with pieces and their starting positions.
     */
    static void addPieces() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                figures[i][j] = null;
            }
        }

        // Add pawns with images
        figureList.add(aw);
        figureList.add(bw);
        figureList.add(cw);
        figureList.add(dw);
        figureList.add(ew);
        figureList.add(fw);
        figureList.add(gw);
        figureList.add(hw);

        figureList.add(r1w);
        figureList.add(r2w);
        figureList.add(b1w);
        figureList.add(b2w);
        figureList.add(k1w);
        figureList.add(k2w);
        figureList.add(kw);
        figureList.add(qw);

        figureList.add(ab);
        figureList.add(bb);
        figureList.add(cb);
        figureList.add(db);
        figureList.add(eb);
        figureList.add(fb);
        figureList.add(gb);
        figureList.add(hb);

        figureList.add(r1b);
        figureList.add(r2b);
        figureList.add(b1b);
        figureList.add(b2b);
        figureList.add(k1b);
        figureList.add(k2b);
        figureList.add(kb);
        figureList.add(qb);

        figures[0][1] = aw;
        figures[1][1] = bw;
        figures[2][1] = cw;
        figures[3][1] = dw;
        figures[4][1] = ew;
        figures[5][1] = fw;
        figures[6][1] = gw;
        figures[7][1] = hw;

        figures[7][0] = r1w;
        figures[0][0] = r2w;
        figures[5][0] = b1w;
        figures[2][0] = b2w;
        figures[6][0] = k1w;
        figures[1][0] = k2w;
        figures[4][0] = kw;
        figures[3][0] = qw;

        figures[0][6] = ab;
        figures[1][6] = bb;
        figures[2][6] = cb;
        figures[3][6] = db;
        figures[4][6] = eb;
        figures[5][6] = fb;
        figures[6][6] = gb;
        figures[7][6] = hb;

        figures[7][7] = r1b;
        figures[0][7] = r2b;
        figures[5][7] = b1b;
        figures[2][7] = b2b;
        figures[6][7] = k1b;
        figures[1][7] = k2b;
        figures[4][7] = kb;
        figures[3][7] = qb;

    }

    /**
     * Handles the movement of chess figures based on user input.
     *
     * @param clickedX The column index of the clicked position on the chessboard.
     * @param clickedY The row index of the clicked position on the chessboard.
     * @throws IOException            If an I/O error occurs while communicating with the server.
     * @throws InterruptedException   If the thread is interrupted during execution.
     */
    static void movingWithFigures(int clickedX, int clickedY) throws IOException, InterruptedException {
        if (clickedY < 8 && Client.isClientTurn) {
            for (Figure figure : figureList) {
                if (figure.color == whoPlays
                        && (gameType == 0 || gameType == 1 || (gameType == 2 && whoPlays == watchingPosition))) {
                    if (clickedX == figure.getPositionX() && clickedY == figure.getPositionY()
                            && figure.selected) {
                        figure.selected = false;
                        existsSelected = false;
                        panel.repaint();
                        break;
                    }
                    if (figure.selected) {
                        if (gameType == 0 || gameType == 2) {
                            if (figure.validateMove(clickedX, clickedY)) {
                                lastMoveColoring[0] = figure.positionX;
                                lastMoveColoring[1] = figure.positionY;
                                figure.move(clickedX, clickedY, "x");
                                lastMoveColoring[2] = clickedX;
                                lastMoveColoring[3] = clickedY;
                                figure.selected = false;
                                existsSelected = false;
                                if (wantToChangeWiew == true) {
                                    if (watchingPosition == "white") {
                                        watchingPosition = "black";
                                    } else {
                                        watchingPosition = "white";
                                    }
                                }
                                if (gameType == 2) {
                                    // Call what bot plays
                                    int[] lastMove = new int[2];
                                    lastMove[0] = clickedX;
                                    lastMove[1] = clickedY;
                                    panel.repaint();
                                    Thread botThread = new Thread(() -> {
                                        String x;
                                        try {
                                            x = Bot.sendBoBot(colorOfBot, figureList,
                                                    lastMove, false);
                                            if (x != "") {
                                                int[] y = parsePosition(x);
                                                Figure figureBotSaver = null;
                                                for (Figure figureBot : figureList) {
                                                    if (y[0] == figureBot.getPositionX()
                                                            && y[1] == figureBot.getPositionY()) {
                                                        figureBotSaver = figureBot;
                                                    }
                                                }
                                                if (y.length > 4 && y[4] == 1) {
                                                    figureBotSaver.move(y[2], y[3], "xqueen");
                                                } else {
                                                    figureBotSaver.move(y[2], y[3], "x");
                                                }
                                                lastMoveColoring[0] = y[0];
                                                lastMoveColoring[1] = y[1];
                                                lastMoveColoring[2] = y[2];
                                                lastMoveColoring[3] = y[3];
                                                panel.repaint();
                                            }
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    });

                                    botThread.start();

                                }
                                panel.repaint();
                                break;
                            }
                        }
                        if (gameType == 1) {
                            if (figure.validateMove(clickedX, clickedY)) {

                                PrintWriter output = new PrintWriter(socket.getOutputStream(), true);

                                int oldPositionX = figure.getPositionX();
                                int oldPositionY = figure.getPositionY();
                                lastMoveColoring[0] = figure.positionX;
                                lastMoveColoring[1] = figure.positionY;
                                String x = figure.move(clickedX, clickedY, "x");
                                lastMoveColoring[2] = clickedX;
                                lastMoveColoring[3] = clickedY;
                                sendMessage(oldPositionX + " " + oldPositionY + " "
                                        + clickedX + " " + clickedY + " x" + x, output);
                                Client.isClientTurn = false;
                                synchronized (lock) {
                                    lock.notifyAll();
                                }
                                figure.selected = false;
                                existsSelected = false;
                                panel.repaint();
                                break;
                            }
                        }
                    }
                    if (clickedX == figure.getPositionX() && clickedY == figure.getPositionY()
                            && !figure.selected) {
                        for (Figure figure2 : figureList) {
                            figure2.selected = false;
                        }
                        figure.setPossibleMoves(true, figures, true);
                        figure.selected = true;
                        existsSelected = true;
                        panel.repaint();
                        break;
                    }
                }

            }
        }
        panel.repaint();
    }

    /**
     * Draws a circle to highlight a possible move on the chessboard.
     *
     * @param i The row index of the possible move.
     * @param j The column index of the possible move.
     * @param g The Graphics object to paint on the panel.
     */
    private static void drawPossibleMoves(int i, int j, Graphics g) {
        int squareSize = panel.getWidth() / 8;
        g.setColor(Color.WHITE);
        if (watchingPosition.equals("black"))
            g.drawOval((7 - i) * squareSize + squareSize / 4, j * squareSize + squareSize / 4, squareSize / 2,
                    squareSize / 2);
        else
            g.drawOval(i * squareSize + squareSize / 4, (7 - j) * squareSize + squareSize / 4, squareSize / 2,
                    squareSize / 2);
    }

    /**
     * Converts mouse event coordinates to chessboard position.
     *
     * @param e The MouseEvent containing the mouse click coordinates.
     * @return An array containing the row and column indices of the clicked position on the chessboard.
     */
    static int[] getPositionOnBoard(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        int squareSize = panel.getWidth() / 8;

        int clickedY;
        int clickedX;

        if (watchingPosition.equals("white")) {
            clickedY = 7 - y / squareSize;
            clickedX = x / squareSize;
        } else {
            clickedY = y / squareSize;
            clickedX = 7 - x / squareSize;
        }
        return new int[]{clickedX, clickedY};
    }


    /**
     * Paints the chessboard and chess pieces on the panel.
     *
     * @param gridSize   The number of squares per row/column on the chessboard.
     * @param squareSize The size of each square in pixels.
     * @param g          The Graphics object to paint on the panel.
     */
    static void paintBoard(int gridSize, int squareSize, Graphics g) {
        for (row = 0; row < gridSize; row++) {
            for (col = 0; col < gridSize; col++) {

                int x = col * squareSize;
                int y = row * squareSize;

                if ((row + col) % 2 == 1) {
                    g.setColor(DARK);
                } else {
                    g.setColor(LIGHT);
                }

                g.fillRect(x, y, squareSize, squareSize);
            }
        }

        // Highlight last move and selected figure
        choosingFigure(g, squareSize);

        // Highlight possible moves for the selected figure
        for (Figure figure : figureList) {
            if (figure.selected) {
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        if (figure.possibleMoves[i][j] == 1) {
                            drawPossibleMoves(i, j, g);
                        }
                    }
                }
            }
        }
    }


    /**
     * Configures the panel settings based on the type of chess game.
     * Adjusts the panel size and background color accordingly.
     *
     * @param chessboardSize The size of the chessboard in pixels.
     * @param frame          The JFrame to which the panel is added.
     */
    static void panelSetting(int chessboardSize, JFrame frame) {
        if (gameType == 0 || gameType == 1) {
            // Hot seat or online game panel settings
            panel.setPreferredSize(new Dimension(chessboardSize, 900));
        } else if (gameType == 2) {
            // Bot game panel settings
            panel.setPreferredSize(new Dimension(chessboardSize, chessboardSize));
        }
        panel.setBackground(LIGHT);
        frame.getContentPane().add(panel);
        frame.pack();
        frame.add(panel);
        frame.setVisible(true);
    }


    /**
     * Determines the type of chess game based on user input.
     * Handles configurations and setups for hot seat, online, and bot games.
     *
     * @throws UnknownHostException When the specified host is not known.
     * @throws IOException          When an I/O exception occurs.
     * @throws NumberFormatException When there is an invalid number format.
     * @throws InterruptedException When a thread is waiting, sleeping, or otherwise occupied.
     */
    static void typeOfTheGameScanner()
            throws UnknownHostException, IOException, NumberFormatException, InterruptedException {

        System.out.println("What type of game do you want: ");
        System.out.println("0 - hot seat");
        System.out.println("1 - online game");
        System.out.println("2 - bot game");

        while (true) {
            try {
                gameType = Integer.parseInt(waitForUserInput());
                if (gameType == 0 || gameType == 1 || gameType == 2) {
                    break;
                }
            } catch (NumberFormatException e) {

            }
        }

        System.out.println("");

        if (gameType == 0) {
            Client.isClientTurn = true;
            watchingPosition = "white";
            System.out.println("You chose hot seat game");
            System.out.println("How many minutes do you want: ");

            while (true) {
                try {
                    minutes = Integer.parseInt(waitForUserInput());
                    if (minutes > 0) {
                        break;
                    }
                } catch (NumberFormatException e) {

                }
            }

            System.out.println("");
            System.out.println("How many extra seconds per move do you want: ");

            while (true) {
                try {
                    secondsPerMove = Integer.parseInt(waitForUserInput());
                    if (secondsPerMove >= 0) {
                        break;
                    }
                } catch (NumberFormatException e) {

                }
            }

            System.out.println("");
            System.out.println("Would you like to change wiew when playing hot seat (1=yes/0=no): ");

            int x;

            while (true) {
                try {
                    x = Integer.parseInt(waitForUserInput());
                    if (x == 0 || x == 1) {
                        break;
                    }
                } catch (NumberFormatException e) {

                }
            }

            System.out.println("");
            if (x == 1) {
                wantToChangeWiew = true;
            } else {
                wantToChangeWiew = false;
            }

        }
        if (gameType == 1) {

            System.out.println("You chose online game");

            System.out.println("Enter the code (larger than 0): ");

            while (true) {
                try {
                    code = Integer.parseInt(waitForUserInput());
                    if (code > 0) {
                        break;
                    }
                } catch (NumberFormatException e) {

                }
            }

            System.out.println("");

            System.out.println("Waiting for other player to join...");

            System.out.println("");

            socket = new Socket("localhost", 12345);

            // Send the client code to the server
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(code);

            Scanner inputForBool = new Scanner(new InputStreamReader(Main.socket.getInputStream()));

            Client.isClientTurn = inputForBool.nextBoolean();

            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);

            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            clientColor(output, input);

            ThreadForWaiting threadForWaiting = new ThreadForWaiting();
            threadForWaiting.start();

        }
        if (gameType == 2) {
            System.out.println("You chose bot game");
            Random random = new Random();
            int randomNumber = random.nextInt(2);
            if (randomNumber == 0) {
                Client.isClientTurn = true;
                watchingPosition = "white";
                colorOfBot = "black";
            } else {
                Client.isClientTurn = false;
                watchingPosition = "black";
                colorOfBot = "white";
            }
            System.out.println("You are " + watchingPosition);

            if (colorOfBot == "white") {
                // Call what bot plays

                int[] lastMove = new int[2];
                lastMove[0] = -1;
                lastMove[1] = -1;
                String x = Bot.sendBoBot(colorOfBot, figureList, lastMove, true);
                int[] y = parsePosition(x);

                for (Figure figureBot : figureList) {
                    if (y[0] == figureBot.getPositionX()
                            && y[1] == figureBot.getPositionY()) {
                        figureBot.move(y[2], y[3], "x");
                    }
                }
                lastMoveColoring[0] = y[0];
                lastMoveColoring[1] = y[1];
                lastMoveColoring[2] = y[2];
                lastMoveColoring[3] = y[3];
                Client.isClientTurn = true;
                panel.repaint();
            }
        }
    }

    /**
     * Highlights and displays the selected figure on the chessboard.
     * Also displays the last move made by coloring the corresponding squares.
     *
     * @param g          The Graphics object for rendering.
     * @param squareSize The size of a chessboard square in pixels.
     */
    static void choosingFigure(Graphics g, int squareSize) {
        // Set the transparency level for the highlighting color
        float alpha = 0.3f;
        Color transparentYellow = new Color(1.0f, 1.0f, 0.0f, alpha);

        g.setColor(transparentYellow);

        // Highlight the squares of the last move made
        if (watchingPosition.equals("white")) {
            if (!(lastMoveColoring[0] == lastMoveColoring[2] && lastMoveColoring[1] == lastMoveColoring[3])) {
                g.fillRect(lastMoveColoring[0] * 100, (7 - lastMoveColoring[1]) * 100,
                        squareSize, squareSize);
                g.fillRect(lastMoveColoring[2] * 100, (7 - lastMoveColoring[3]) * 100,
                        squareSize, squareSize);
            }
        } else {
            if (!(lastMoveColoring[0] == lastMoveColoring[2] && lastMoveColoring[1] == lastMoveColoring[3])) {
                g.fillRect((7 - lastMoveColoring[0]) * 100, lastMoveColoring[1] * 100,
                        squareSize, squareSize);
                g.fillRect((7 - lastMoveColoring[2]) * 100, lastMoveColoring[3] * 100,
                        squareSize, squareSize);
            }
        }

        // Iterate through the list of figures and highlight the selected figure
        for (Figure figure : figureList) {
            if (watchingPosition.equals("white")) {
                if (figure.selected) {
                    // Highlight the selected figure with gray color
                    g.setColor(Color.GRAY);
                    g.fillRect(figure.getPositionX() * 100, (7 - figure.getPositionY()) * 100,
                            squareSize, squareSize);
                    // Draw the selected figure
                    g.drawImage(figure.getFigureImage(), figure.getPositionX() * 100,
                            (7 - figure.getPositionY()) * 100, squareSize, squareSize, panel);
                }
                // Draw the figure on the chessboard
                g.drawImage(figure.getFigureImage(), figure.getPositionX() * 100,
                        (7 - figure.getPositionY()) * 100, squareSize, squareSize, panel);
            } else {
                if (figure.selected) {
                    // Highlight the selected figure with gray color
                    g.setColor(Color.GRAY);
                    g.fillRect((7 - figure.getPositionX()) * 100, figure.getPositionY() * 100,
                            squareSize, squareSize);
                    // Draw the selected figure
                    g.drawImage(figure.getFigureImage(), (7 - figure.getPositionX()) * 100,
                            figure.getPositionY() * 100, squareSize, squareSize, panel);
                }
                // Draw the figure on the chessboard
                g.drawImage(figure.getFigureImage(), (7 - figure.getPositionX()) * 100,
                        figure.getPositionY() * 100, squareSize, squareSize, panel);
            }
        }
    }


    /**
     * Waits for user input by acquiring a semaphore.
     * This method blocks until the semaphore is acquired, indicating that user input is available.
     *
     * @return The user input as a String.
     * @throws InterruptedException If the thread is interrupted while waiting for user input.
     */
    public static String waitForUserInput() throws InterruptedException {
        // Acquire the semaphore to wait for user input
        inputSemaphore.acquire();
        // Return the user input
        return userInput;
    }


    /**
     * Creates and initializes the input field components for user interaction.
     * Sets up a JFrame with a text area for displaying messages and a JTextField for user input.
     * Captures user input and releases a semaphore to signal that input is ready.
     */
    static void inputField() {
        // Create the input frame
        frameInput = new JFrame("");
        frameInput.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameInput.setSize(800, 800);
        frameInput.setResizable(false);
        frameInput.setLayout(new BorderLayout());

        // Create text area
        textArea = new JTextArea();
        textArea.setEditable(false);
        stream = new PrintStream(new TextAreaOutputStream(textArea));

        // Redirect standard output and error streams to the text area
        System.setOut(stream);
        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        frameInput.add(new JScrollPane(textArea), BorderLayout.CENTER);
        textArea.requestFocusInWindow();

        // Create an input field to send messages
        inputField = new JTextField();
        frameInput.add(inputField, BorderLayout.SOUTH);

        // Set font and color for input field and text area
        inputField.setFont(new Font("Serif", Font.BOLD, 20));
        inputField.setForeground(Color.WHITE);
        inputField.setBackground(DARK);
        textArea.setFont(new Font("Serif", Font.BOLD, 20));
        textArea.setForeground(Color.WHITE);
        textArea.setBackground(DARK);
        frameInput.getContentPane().setBackground(DARK);
        frameInput.getContentPane().setForeground(DARK);

        // Add a key listener to the input field to capture Enter key press
        inputField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userInput = inputField.getText();
                System.out.println("You: " + userInput); // Display user input
                // Release the semaphore to indicate that user input is ready
                inputSemaphore.release();
                inputField.setText(""); // Clear the input field
            }
        });

        // Make the input frame visible
        frameInput.setVisible(true);
    }


    /**
     * Displays an end game message in a separate JFrame based on the provided color.
     *
     * @param color The color indicating the winner or "draw" for a draw game.
     *              Possible values: "white", "black", or "draw".
     */
    static void end(String color) {
        // Create a new JFrame for the end game message
        frameEnd = new JFrame("");
        frameEnd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameEnd.setSize(800, 800);
        frameEnd.setResizable(false);
        frameEnd.setLayout(new BorderLayout());

        // Create a JLabel to display the end game message
        JLabel label;
        if (color.equals("white")) {
            label = new JLabel("White wins!");
        } else if (color.equals("black")) {
            label = new JLabel("Black wins!");
        } else {
            label = new JLabel("It is a draw!");
        }

        // Set the appearance of the JLabel
        label.setFont(new Font("Arial", Font.PLAIN, 50));
        label.setForeground(Color.WHITE);
        label.setBackground(DARK);

        // Set the appearance of the JFrame
        frameEnd.getContentPane().setBackground(DARK);
        frameEnd.getContentPane().setForeground(DARK);
        frameEnd.setBackground(DARK);

        // Set label alignment and add it to the JFrame
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);
        frameEnd.add(label);

        // Make the JFrame visible
        if (!frameEnd.isVisible()) {
            frameEnd.setVisible(true);
        }
    }


    /**
     * Loads chess piece images from files into corresponding Image variables.
     * The images are loaded for both black and white pieces.
     * Replace the placeholder file paths with the actual paths to the images.
     */
    static void loadImages() {
        try {
            // Load images for black pieces
            bishopBlack = ImageIO.read(new File("./images/bishop_black.png"));
            kingBlack = ImageIO.read(new File("./images/king_black.png"));
            knightBlack = ImageIO.read(new File("./images/knight_black.png"));
            pawnBlack = ImageIO.read(new File("./images/pawn_black.png"));
            queenBlack = ImageIO.read(new File("./images/queen_black.png"));
            rookBlack = ImageIO.read(new File("./images/rook_black.png"));

            // Load images for white pieces
            bishopWhite = ImageIO.read(new File("./images/bishop_white.png"));
            kingWhite = ImageIO.read(new File("./images/king_white.png"));
            knightWhite = ImageIO.read(new File("./images/knight_white.png"));
            pawnWhite = ImageIO.read(new File("./images/pawn_white.png"));
            queenWhite = ImageIO.read(new File("./images/queen_white.png"));
            rookWhite = ImageIO.read(new File("./images/rook_white.png"));

        } catch (IOException e) {
            // Handle IOException by printing stack trace and error message
            e.printStackTrace();
            System.err.println("IOException: " + e.getMessage());
        }
    }


    /**
     * The main method for the Chess game.
     * @param args Command-line arguments (not used).
     * @throws UnknownHostException If an error occurs while resolving the host.
     * @throws IOException If an I/O error occurs.
     * @throws InterruptedException If the execution is interrupted.
     */
    public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {

        // Set up initial input field, add pieces, and determine the type of the game
        inputField();
        addPieces();
        typeOfTheGameScanner();
        printStartingInfo();

        // Create a unique string representation of the initial position and store it in the map
        String positionString = Arrays.deepToString(figures);
        positionCountMapWhite.put(positionString, 1);

        // Run the GUI on the Event Dispatch Thread
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Set up the main JFrame for the chess game
                frame = new JFrame("Chess");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setResizable(false);

                int chessboardSize = 800; // Size of the chessboard in pixels

                // Create a custom JPanel for rendering the chessboard
                panel = new JPanel(null) {
                    @Override
                    public void paintComponent(Graphics g) {
                        super.paintComponent(g);

                        int gridSize = 8; // Number of squares per row/column
                        int squareSize = getWidth() / gridSize;

                        // Paint the chessboard grid
                        paintBoard(gridSize, squareSize, g);
                    }
                };

                // If the game type is human vs human or human vs AI, set up the timer
                if (gameType == 0 || gameType == 1) {
                    timer(panel, frame);
                }

                // Add mouse listener to the panel for handling user clicks
                panel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        // Get the position on the board where the user clicked
                        int[] positionOnBoard = getPositionOnBoard(e);
                        int clickedX = positionOnBoard[0];
                        int clickedY = positionOnBoard[1];

                        System.out.println("Position where was clicked: " + clickedX + clickedY);

                        try {
                            // Handle user's move with figures
                            movingWithFigures(clickedX, clickedY);
                        } catch (IOException | InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                });

                // Set up the appearance and layout of the panel
                panelSetting(chessboardSize, frame);
            }
        });
    }


    /**
     * Sets up and starts a timer for both White and Black players.
     * @param panel The JPanel where the timer labels will be added.
     * @param frame The JFrame containing the game.
     */
    public static void timer(JPanel panel, JFrame frame) {
        JLabel timerLabelWhite = new JLabel(String.format("White: %02d:%02d",
                minutes, 0));
        panel.add(timerLabelWhite, BorderLayout.SOUTH);

        timerLabelWhite.setBounds(30, 830, 200, 30);

        timerLabelWhite.setFont(new Font("Serif", Font.BOLD, 20));
        timerLabelWhite.setForeground(Color.WHITE);

        frame.add(panel);
        secondsRemainingWhite = minutes * 60;
        timerWhite = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (secondsRemainingWhite >= 0) {
                    secondsRemainingWhite--;
                    boolean draw = false;
                    if (secondsRemainingWhite == 0) {
                        if (figureList.size() == 2) {
                            draw = true;
                        }
                        if (figureList.size() == 3) {
                            for (Figure figure : figureList) {
                                if ((figure instanceof Knight || figure instanceof Bishop) && figure.color == "white") {
                                    draw = true;
                                }
                            }
                        }
                        if (figureList.size() == 4) {
                            for (Figure figure : figureList) {
                                if (figure instanceof Bishop && figure.color == "white") {
                                    for (Figure figure2 : figureList) {
                                        if (figure2 instanceof Bishop && figure2.color == "black"
                                                && figure.bishopColor == figure2.bishopColor) {
                                            draw = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (draw) {
                            end("draw");
                        } else {
                            end("black");
                        }
                    }
                    int minutes = secondsRemainingWhite / 60;
                    int seconds = secondsRemainingWhite % 60;
                    timerLabelWhite.setText(String.format("White: %02d:%02d", minutes,
                            seconds));
                }
            }
        });

        JLabel timerLabelBlack = new JLabel(String.format("Black: %02d:%02d",
                minutes, 0));
        panel.add(timerLabelBlack, BorderLayout.SOUTH);

        timerLabelBlack.setBounds(650, 830, 200, 30);

        timerLabelBlack.setFont(new Font("Serif", Font.BOLD, 20));
        timerLabelBlack.setForeground(Color.WHITE);

        frame.add(panel);
        secondsRemainingBlack = minutes * 60;
        timerBlack = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (secondsRemainingBlack >= 0) {
                    secondsRemainingBlack--;
                    boolean draw = false;
                    if (secondsRemainingBlack == 0) {
                        if (figureList.size() == 2) {
                            draw = true;
                        }
                        if (figureList.size() == 3) {
                            for (Figure figure : figureList) {
                                if ((figure instanceof Knight || figure instanceof Bishop) && figure.color == "black") {
                                    draw = true;
                                }
                            }
                        }
                        if (figureList.size() == 4) {
                            for (Figure figure : figureList) {
                                if (figure instanceof Bishop && figure.color == "black") {
                                    for (Figure figure2 : figureList) {
                                        if (figure2 instanceof Bishop && figure2.color == "white"
                                                && figure.bishopColor == figure2.bishopColor) {
                                            draw = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (draw) {
                            end("draw");
                        } else {
                            end("white");
                        }
                    }
                    int minutes = secondsRemainingBlack / 60;
                    int seconds = secondsRemainingBlack % 60;
                    timerLabelBlack.setText(String.format("Black: %02d:%02d", minutes,
                            seconds));
                }
            }
        });

        timerWhite.start();

    }

    /**
     * A thread class used for waiting and handling incoming moves from the server.
     */
    public static class ThreadForWaiting extends Thread {

        /**
         * Overrides the run method of the Thread class to implement the custom logic for waiting and handling moves.
         */
        @Override
        public void run() {
            while (true) {
                try {
                    // Synchronize on the lock object
                    synchronized (lock) {
                        // Wait while it's not the client's turn
                        while (Client.isClientTurn) {
                            lock.wait();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // Check if it's now the client's turn
                if (!Client.isClientTurn) {
                    try {
                        // Wait for the incoming message containing the opponent's move
                        String moveMessage = waitForMessage(new BufferedReader(new InputStreamReader(socket.getInputStream())));

                        // Split the message to extract move details
                        String[] moveDetails = moveMessage.split(" ");
                        int oldPositionX = Integer.parseInt(moveDetails[0]);
                        int oldPositionY = Integer.parseInt(moveDetails[1]);
                        int newPositionX = Integer.parseInt(moveDetails[2]);
                        int newPositionY = Integer.parseInt(moveDetails[3]);

                        String y = moveDetails[4];

                        // Find and move the corresponding figure in the client's figure list
                        for (Figure figure : figureList) {
                            if (figure.getPositionX() == oldPositionX && figure.getPositionY() == oldPositionY) {
                                figure.move(newPositionX, newPositionY, y);
                                break;
                            }
                        }

                        // Update last move coloring details
                        lastMoveColoring[0] = oldPositionX;
                        lastMoveColoring[1] = oldPositionY;
                        lastMoveColoring[2] = newPositionX;
                        lastMoveColoring[3] = newPositionY;

                        // Repaint the panel to reflect the updated state
                        panel.repaint();

                        // Set it to the client's turn
                        Client.isClientTurn = true;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    /**
     * Parses a position string in the format "x,y" into an array of integers.
     * Non-numeric characters are removed before parsing.
     */
    static int[] parsePosition(String positionString) {
        // Split the input string into parts using ","
        String[] parts = positionString.split(",");
        
        // Create an array to store parsed integers
        int[] positions = new int[parts.length];

        // Iterate over the parts and parse each as an integer after removing non-numeric characters
        for (int i = 0; i < parts.length; i++) {
            // Remove any non-numeric characters (e.g., "-->") and then parse as an integer
            positions[i] = Integer.parseInt(parts[i].replaceAll("[^0-9-]", ""));
        }

        // Return the array of parsed integers
        return positions;
    }

}

/**
 * An OutputStream implementation that writes the output to a JTextArea.
 */
class TextAreaOutputStream extends java.io.OutputStream {
    private JTextArea textArea;

    /**
     * Constructs a new TextAreaOutputStream with the specified JTextArea.
     *
     * @param textArea The JTextArea to which the output will be written.
     */
    public TextAreaOutputStream(JTextArea textArea) {
        this.textArea = textArea;
    }

    /**
     * Writes the specified byte to the JTextArea.
     *
     * @param b The byte to be written.
     * @throws IOException If an I/O error occurs.
     */
    @Override
    public void write(int b) throws java.io.IOException {
        // Append the character representation of the byte to the JTextArea
        textArea.append(String.valueOf((char) b));

        // Set the caret position to the end of the JTextArea
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }
}