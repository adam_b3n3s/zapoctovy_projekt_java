package cz.cuni.mff.benesada;

/**
 * Represents a Queen chess piece.
 */
public class Queen extends Figure {

    /**
     * Constructor for the Queen class.
     *
     * @param positionX The initial X-coordinate of the Queen on the chessboard.
     * @param positionY The initial Y-coordinate of the Queen on the chessboard.
     * @param color The color of the Queen ("white" or "black").
     */
    public Queen(int positionX, int positionY, String color) {
        super(positionX, positionY, color, "queen");
        super.value = 9; // Setting the value of the Queen piece.
    }

    /**
     * Moves the Queen to a new position and prints the move details.
     *
     * @param newPositionX The new X-coordinate for the Queen.
     * @param newPositionY The new Y-coordinate for the Queen.
     * @param wasTherePromotion A string indicating if there was a pawn promotion during the move.
     * @return The result of the move operation.
     */
    @Override
    public String move(int newPositionX, int newPositionY, String wasTherePromotion) {
        System.out.println("Queen moves to (" + newPositionX + ", " + newPositionY + ")");
        return super.move(newPositionX, newPositionY, wasTherePromotion);
    }
}
