package cz.cuni.mff.benesada;

import java.util.Map;

/**
 * Represents a King chess piece.
 */
public class King extends Figure {

    /**
     * Constructor for the King class.
     *
     * @param positionX The initial X-coordinate of the King on the chessboard.
     * @param positionY The initial Y-coordinate of the King on the chessboard.
     * @param color The color of the King ("white" or "black").
     */
    public King(int positionX, int positionY, String color) {
        super(positionX, positionY, color, "king");
    }

    /**
     * Moves the King to a new position and prints the move details.
     *
     * @param newPositionX The new X-coordinate for the King.
     * @param newPositionY The new Y-coordinate for the King.
     * @param wasTherePromotion A string indicating if there was a pawn promotion during the move.
     * @return The result of the move operation.
     */
    @Override
    public String move(int newPositionX, int newPositionY, String wasTherePromotion) {
        System.out.println("King moves to (" + newPositionX + ", " + newPositionY + ")");
        return super.move(newPositionX, newPositionY, wasTherePromotion);
    }

    /**
     * Checks for checkmate, stalemate, or draw conditions in the current game state.
     *
     * @param fiftyMoveCounter The counter for the fifty-move rule.
     * @param figures The 2D array representing the current state of the chessboard.
     * @param positionCountMapWhite Map containing counts of each piece's position for white.
     * @param positionCountMapBlack Map containing counts of each piece's position for black.
     * @return A string indicating the result of the game ("n" for no result, "d" for draw, "y" for checkmate).
     */
    public String checkmate(int fiftyMoveCounter, Figure[][] figures,
            Map<String, Integer> positionCountMapWhite, Map<String, Integer> positionCountMapBlack) {

        for (Integer count : positionCountMapWhite.values()) {
            if (count >= 3) {
                return "d";
            }
        }
        for (Integer count : positionCountMapBlack.values()) {
            if (count >= 3) {
                return "d";
            }
        }

        if (fiftyMoveCounter >= 100) {
            return "d";
        }
        
        if (drawByMaterial(figures) == true)
        {
            return "d";
        }

        this.setPossibleMoves(false, figures, true);

        if (hasMove(this.possibleMoves)) {
            return "n";
        }

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (figures[i][j] != null && figures[i][j].color == this.color) {
                    figures[i][j].setPossibleMoves(false, figures, true);
                    if (hasMove(figures[i][j].possibleMoves)) {
                        return "n";
                    }
                }
            }
        }

        if (this.isInCheck(figures) == true) {
            return "y";
        }
        if (this.isInCheck(figures) == false) {
            return "d";
        }
        return "n";
    }

    /**
     * Checks if the game is drawn due to insufficient material on the chessboard.
     *
     * @param figures The 2D array representing the current state of the chessboard.
     * @return True if the game is drawn due to insufficient material, false otherwise.
     */
    boolean drawByMaterial(Figure[][] figures) {
        int size = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (figures[i][j] != null) {
                    size++;
                }
            }
        }
        if (size > 4) {
            return false;
        }
        if (size == 2) {
            return true;
        }
        if (size == 3) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (figures[i][j] != null && (figures[i][j] instanceof Knight || figures[i][j] instanceof Bishop)) {
                        return true;
                    }
                }
            }
        }
        if (size == 4) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (figures[i][j] != null && figures[i][j] instanceof Bishop) {
                        for (int k = 0; k < 8; k++) {
                            for (int l = 0; l < 8; l++) {
                                if (figures[k][l] != null && figures[k][l] instanceof Bishop) {
                                    if (figures[i][j].color != figures[k][l].color) {
                                        if (figures[i][j].bishopColor == figures[k][l].bishopColor) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks if the King is currently in check.
     *
     * @param figures The 2D array representing the current state of the chessboard.
     * @return True if the King is in check, false otherwise.
     */
    public boolean isInCheck(Figure[][] figures) {
        boolean x = Figure.checkIfOpponentSeesThisSquare(this.getPositionX(), this.getPositionY(), figures,
                this.color);
        if (x == true) {
            Bot.wasThereCaptureOrCheck = true;
        }
        return x;
    }

    /**
     * Checks if the King has any legal moves.
     *
     * @param possibleMoves The 2D array representing the possible moves for the King.
     * @return True if the King has legal moves, false otherwise.
     */
    public static boolean hasMove(int[][] possibleMoves) {
        if (possibleMoves == null) {
            return true;
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (possibleMoves[i][j] == 1) {
                    return true;
                }
            }
        }
        return false;
    }

}
