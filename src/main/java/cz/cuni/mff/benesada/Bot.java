package cz.cuni.mff.benesada;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * The Bot class represents the logic for making moves in a chess game.
 * It includes methods for generating moves, evaluating positions, and making decisions using the minimax algorithm.
 */
public class Bot {
    static int HOW_DEEP = 4;
    static int MAX_HOW_DEEP_LOWER = 4;
    static int HOW_DEEP_LOWER = 0;

    static boolean prom = false;
    public static boolean wasThereCaptureOrCheck = false;

    /**
     * Generates a move for the bot based on the current game state.
     *
     * @param colorOfBot        The color of the bot ('white' or 'black').
     * @param figureList        The list of chess figures on the board.
     * @param lastMove           The last move made in the game.
     * @param isFirstMoveEver    Indicates whether it is the first move of the game.
     * @return A string representing the bot's move.
     * @throws InterruptedException Thrown if the execution is interrupted.
     */
    static String sendBoBot(String colorOfBot, ArrayList<Figure> figureList,
            int[] lastMove, boolean isFirstMoveEver) throws InterruptedException {
        if (isFirstMoveEver == true) {
            String[] scriptedMoves = {
                    "4,1,4,3",
                    "3,1,3,3",
                    "1,0,2,2",
                    "6,0,5,2"
            };
            Random random = new Random();
            int randomIndex = random.nextInt(scriptedMoves.length);
            return scriptedMoves[randomIndex];
        }

        int size = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (Main.figures[i][j] != null) {
                    size++;
                }
            }
        }

        if (size <= 5) {
            HOW_DEEP = 4;
        }

        if (size <= 4) {
            HOW_DEEP = 5;
        }
        if (size <= 3) {
            HOW_DEEP = 5;
        }

        String bestMove = "";
        try {
            bestMove = minimax(colorOfBot, HOW_DEEP, -10000, 10000, true,
                    deepCopyFigures(Main.figures), Main.moveCounter, 0, Main.positionCountMapWhite,
                    Main.positionCountMapWhite);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("Best move: " + bestMove);
        return bestMove;
    }

    /**
     * The minimax algorithm for finding the best move based on the current game state.
     *
     * @param colorOfBot           The color of the bot ('white' or 'black').
     * @param depth                The depth of the search in the game tree.
     * @param alpha                The alpha value for alpha-beta pruning.
     * @param beta                 The beta value for alpha-beta pruning.
     * @param maximizingPlayer     Indicates whether the bot is the maximizing player.
     * @param figures              The current configuration of chess figures on the board.
     * @param moveCounter          The current move counter.
     * @param goToMinus            The depth value to reach before stopping the search.
     * @param positionCountMapWhite Map to count positions for white pieces.
     * @param positionCountMapBlack Map to count positions for black pieces.
     * @return The best move as a string or the evaluation score as a string.
     * @throws NumberFormatException Thrown if a numeric conversion error occurs.
     * @throws CloneNotSupportedException Thrown if cloning is not supported.
     */
    public static String minimax(String colorOfBot, int depth, double alpha, int beta,
            boolean maximizingPlayer, Figure[][] figures, int moveCounter, int goToMinus,
            Map<String, Integer> positionCountMapWhite, Map<String, Integer> positionCountMapBlack)
            throws NumberFormatException, CloneNotSupportedException {

        Map<String, Integer> copyWhite = new HashMap<>();

        for (Map.Entry<String, Integer> entry : positionCountMapWhite.entrySet()) {
            copyWhite.put(entry.getKey(), entry.getValue());
        }

        Map<String, Integer> copyBlack = new HashMap<>();

        for (Map.Entry<String, Integer> entry : positionCountMapBlack.entrySet()) {
            copyBlack.put(entry.getKey(), entry.getValue());
        }

        if (depth == goToMinus || -MAX_HOW_DEEP_LOWER >= goToMinus) {
            return Double.toString(validatePosition(colorOfBot, figures, moveCounter, copyWhite,
                    copyBlack));
        }

        if (maximizingPlayer) {
            double maxEval = -100000;
            String bestMove = "";

            ArrayList<String> possibleMoves = generateMoves(colorOfBot, figures);

            for (String move : possibleMoves) {

                Figure[][] newFigures = makeMove(move, deepCopyFigures(figures));

                if (Bot.prom == true) {
                    move = move + ",1";
                }
                Bot.prom = false;

                String positionString = Arrays.deepToString(newFigures);

                if (colorOfBot == "white") {
                    if (copyWhite.containsKey(positionString)) {
                        int count = copyWhite.get(positionString);
                        copyWhite.put(positionString, count + 1);
                    } else {
                        copyWhite.put(positionString, 1);
                    }
                } else {
                    if (copyBlack.containsKey(positionString)) {
                        int count = copyBlack.get(positionString);
                        copyBlack.put(positionString, count + 1);
                    } else {
                        copyBlack.put(positionString, 1);
                    }
                }

                int x = checkmate(colorOfBot, newFigures, copyWhite, copyBlack);
                double eval = 2;
                if (x == 1) {
                    eval = 0;
                } else if (x != 0) {
                    eval = x;
                }

                if (wasThereCaptureOrCheck == true && eval == 2) {
                    wasThereCaptureOrCheck = false;
                    eval = Double.parseDouble(minimax(colorOfBot, depth - 1, alpha, beta, false,
                            deepCopyFigures(newFigures), moveCounter + 1, goToMinus - HOW_DEEP_LOWER,
                            copyWhite, copyBlack));
                } else if (eval == 2) {
                    eval = Double.parseDouble(minimax(colorOfBot, depth - 1, alpha, beta, false,
                            deepCopyFigures(newFigures), moveCounter + 1, goToMinus, copyWhite,
                            copyBlack));
                }

                if (eval > maxEval) {
                    maxEval = eval;
                    bestMove = move;
                }

                alpha = Math.max(alpha, eval);
                if (beta <= alpha) {
                    break;
                }
            }

            if (depth == HOW_DEEP) {
                return bestMove;
            } else {
                return Double.toString(maxEval);
            }
        } else {
            double minEval = 100000;
            String bestMove = "";

            ArrayList<String> possibleMoves;

            if (colorOfBot == "white") {
                possibleMoves = generateMoves("black", figures);
            } else {
                possibleMoves = generateMoves("white", figures);
            }

            for (String move : possibleMoves) {

                Figure[][] newFigures = makeMove(move, deepCopyFigures(figures));

                if (Bot.prom == true) {
                    move = move + ",1";
                }
                Bot.prom = false;

                String positionString = Arrays.deepToString(newFigures);

                if (colorOfBot == "white") {

                    if (copyBlack.containsKey(positionString)) {
                        int count = copyBlack.get(positionString);
                        copyBlack.put(positionString, count + 1);
                    } else {
                        copyBlack.put(positionString, 1);
                    }
                } else {
                    if (copyWhite.containsKey(positionString)) {
                        int count = copyWhite.get(positionString);
                        copyWhite.put(positionString, count + 1);
                    } else {
                        copyWhite.put(positionString, 1);
                    }
                }

                int x;
                if (colorOfBot == "white") {
                    x = checkmate("black", newFigures, copyWhite, copyBlack);
                } else {
                    x = checkmate("white", newFigures, copyWhite, copyBlack);
                }
                double eval = 2;
                if (x == 1) {
                    eval = 0;
                } else if (x != 0) {
                    eval = x;
                }

                if (wasThereCaptureOrCheck == true && eval == 2) {
                    wasThereCaptureOrCheck = false;
                    eval = Double.parseDouble(minimax(colorOfBot, depth - 1, alpha, beta, true,
                            deepCopyFigures(newFigures), moveCounter + 1, goToMinus - HOW_DEEP_LOWER,
                            copyWhite, copyBlack));
                } else if (eval == 2) {
                    eval = Double.parseDouble(minimax(colorOfBot, depth - 1, alpha, beta, true,
                            deepCopyFigures(newFigures), moveCounter + 1, goToMinus, copyWhite,
                            copyBlack));
                }

                if (eval < minEval) {
                    minEval = eval;
                    bestMove = move;
                }

                alpha = Math.min(alpha, eval);
                if (beta <= alpha) {
                    break;
                }
            }

            if (depth == HOW_DEEP) {
                return bestMove;
            } else {
                return Double.toString(minEval);
            }
        }
    }
    
    /**
     * Deep copies the 2D array of chess figures.
     *
     * @param originalFigures The original array to be copied.
     * @return A deep copy of the array.
     * @throws CloneNotSupportedException Thrown if cloning is not supported.
     */
    static Figure[][] deepCopyFigures(Figure[][] originalFigures) throws CloneNotSupportedException {
        Figure[][] newFigures = new Figure[8][8];

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (originalFigures[i][j] != null) {
                    newFigures[i][j] = (Figure) originalFigures[i][j].clone();
                }
            }
        }

        return newFigures;
    }

    /**
     * Generates a list of possible moves for the chess figures of the specified color on the current board.
     *
     * @param colorOfBot The color of the bot ('white' or 'black').
     * @param figures    The current configuration of chess figures on the board.
     * @return An ArrayList containing strings representing possible moves in the format "oldPositionX,oldPositionY,newPositionX,newPositionY".
     */
    static ArrayList<String> generateMoves(String colorOfBot, Figure[][] figures) {
        ArrayList<String> possibleMoves = new ArrayList<String>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (figures[i][j] != null && figures[i][j].color.equals(colorOfBot)) {
                    figures[i][j].setPossibleMoves(true, figures, true);
                    for (int k = 0; k < 8; k++) {
                        for (int l = 0; l < 8; l++) {
                            if (figures[i][j].validateMove(k, l) == true) {
                                possibleMoves.add(i + "," + j + "," + k + "," + l);
                            }
                        }
                    }
                }
            }
        }
        return possibleMoves;
    }

    /**
     * Updates the chessboard configuration based on the provided move string.
     *
     * @param move    The move string in the format "oldPositionX,oldPositionY,newPositionX,newPositionY".
     * @param figures The current configuration of chess figures on the board.
     * @return The updated configuration of chess figures after the move.
     */
    static Figure[][] makeMove(String move, Figure[][] figures) {
        String[] moveArray = move.split(",");
        int oldPositionX = Integer.parseInt(moveArray[0]);
        int oldPositionY = Integer.parseInt(moveArray[1]);
        int newPositionX = Integer.parseInt(moveArray[2]);
        int newPositionY = Integer.parseInt(moveArray[3]);

        figures[newPositionX][newPositionY] = figures[oldPositionX][oldPositionY];

        figures[oldPositionX][oldPositionY] = null;

        figures[newPositionX][newPositionY].positionX = newPositionX;

        figures[newPositionX][newPositionY].positionY = newPositionY;

        if (figures[newPositionX][newPositionY] instanceof Pawn && newPositionY == 7) {
            Bot.prom = true;
            figures[newPositionX][newPositionY] = new Queen(newPositionX, newPositionY, "white");
        } else if (figures[newPositionX][newPositionY] instanceof Pawn && newPositionY == 0) {
            Bot.prom = true;
            figures[newPositionX][newPositionY] = new Queen(newPositionX, newPositionY, "black");
        }

        return figures;
    }

    /**
     * Evaluates the validity of the current chess position based on various criteria.
     *
     * @param colorOfBot           The color of the bot ('white' or 'black').
     * @param figures              The current configuration of chess figures on the board.
     * @param moveCounter          The current move counter.
     * @param positionCountMapWhite Map containing the count of positions for white pieces.
     * @param positionCountMapBlack Map containing the count of positions for black pieces.
     * @return A value indicating the validity of the current position.
     */
    static double validatePosition(String colorOfBot, Figure[][] figures, int moveCounter,
            Map<String, Integer> positionCountMapWhite, Map<String, Integer> positionCountMapBlack) {
        double valid = 0;
        valid += knightsOut(colorOfBot, figures, moveCounter);
        valid += centerFigures(colorOfBot, figures, moveCounter);
        valid += blockedPawns(colorOfBot, figures);
        valid += positionAndSafetyOfKing(colorOfBot, figures);
        valid += material(colorOfBot, figures);
        valid += pawnStructure(colorOfBot, figures);
        valid += colorOfPawns(colorOfBot, figures);
        int x = checkmate(colorOfBot, figures, positionCountMapWhite, positionCountMapBlack);
        if (x == 1) {
            valid = 0;
        } else {
            valid += x;
        }
        return valid;
    }

    /**
     * Evaluates the positioning of knights on the board, providing a bonus or penalty based on their
     * initial moves in the game.
     *
     * @param colorOfBot The color of the bot ('white' or 'black').
     * @param figures    The current configuration of chess figures on the board.
     * @param moveCounter The current move counter.
     * @return A value indicating the bonus or penalty for the positioning of knights.
     */
    static double knightsOut(String colorOfBot, Figure[][] figures, int moveCounter) {
        double value = 0;
        for (int i = 1; i <= 6; i++) {
            for (int j = 2; j <= 5; j++) {
                if (figures[i][j] != null) {
                    if (figures[i][j].color == colorOfBot && figures[i][j] instanceof Knight) {
                        if (moveCounter < 4) {
                            value += 0.4;
                        }
                    } else {
                        if (moveCounter < 4) {
                            value -= 0.4;
                        }
                    }
                }
            }
        }
        return value;
    }

    /**
     * Evaluates the positioning of figures in the central region of the board, providing a bonus or
     * penalty based on the move counter.
     *
     * @param colorOfBot The color of the bot ('white' or 'black').
     * @param figures    The current configuration of chess figures on the board.
     * @param moveCounter The current move counter.
     * @return A value indicating the bonus or penalty for the positioning of figures in the center.
     */
    static double centerFigures(String colorOfBot, Figure[][] figures, int moveCounter) {
        double value = 0;
        for (int i = 2; i <= 5; i++) {
            for (int j = 2; j <= 5; j++) {
                if (figures[i][j] != null) {
                    if (figures[i][j].color == colorOfBot) {
                        if (moveCounter < 5) {
                            value += 0.4;
                        } else {
                            value += 0.2;
                        }
                    } else {
                        if (moveCounter < 5) {
                            value -= 0.4;
                        } else {
                            value -= 0.2;
                        }
                    }
                }
            }
        }
        return value;
    }

    /**
     * Evaluates the presence of blocked pawns on the board, penalizing pawns that are blocked by
     * other pawns of the same color.
     *
     * @param colorOfBot The color of the bot ('white' or 'black').
     * @param figures     The current configuration of chess figures on the board.
     * @return A value indicating the penalty for blocked pawns of the bot's color.
     */
    static double blockedPawns(String colorOfBot, Figure[][] figures) {
        double value = 0;

        // Iterate over each cell on the board
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                // Check if the cell contains a pawn
                if (figures[i][j] != null && figures[i][j] instanceof Pawn) {
                    // Evaluate based on the color of the pawn
                    if (figures[i][j].color.equals("white")) {
                        // Check if the next cell is occupied by a pawn of the same color
                        if (j + 1 < 8 && figures[i][j + 1] != null
                                && figures[i][j + 1].color.equals("white")
                                && figures[i][j + 1] instanceof Pawn) {
                            // Penalize if the next cell is occupied by a white pawn
                            if (colorOfBot.equals("white")) {
                                value -= 0.2;
                            }
                        }
                        // Check if the next two cells are occupied by a pawn of the same color
                        if (j + 2 < 8 && figures[i][j + 2] != null
                                && figures[i][j + 2].color.equals("white")
                                && figures[i][j + 2] instanceof Pawn) {
                            // Penalize if the next two cells are occupied by a white pawn
                            if (colorOfBot.equals("white")) {
                                value -= 0.17;
                            }
                        }
                        // Check if the next three cells are occupied by a pawn of the same color
                        if (j + 3 < 8 && figures[i][j + 3] != null
                                && figures[i][j + 3].color.equals("white")
                                && figures[i][j + 3] instanceof Pawn) {
                            // Penalize if the next three cells are occupied by a white pawn
                            if (colorOfBot.equals("white")) {
                                value -= 0.15;
                            }
                        }
                    } else if (figures[i][j].color.equals("black")) {
                        // Check if the next cell is occupied by a pawn of the same color
                        if (j - 1 >= 0 && figures[i][j - 1] != null
                                && figures[i][j - 1].color.equals("black")
                                && figures[i][j - 1] instanceof Pawn) {
                            // Penalize if the next cell is occupied by a black pawn
                            if (colorOfBot.equals("black")) {
                                value -= 0.2;
                            }
                        }
                        // Check if the next two cells are occupied by a pawn of the same color
                        if (j - 2 >= 0 && figures[i][j - 2] != null
                                && figures[i][j - 2].color.equals("black")
                                && figures[i][j - 2] instanceof Pawn) {
                            // Penalize if the next two cells are occupied by a black pawn
                            if (colorOfBot.equals("black")) {
                                value -= 0.17;
                            }
                        }
                        // Check if the next three cells are occupied by a pawn of the same color
                        if (j - 3 >= 0 && figures[i][j - 3] != null
                                && figures[i][j - 3].color.equals("black")
                                && figures[i][j - 3] instanceof Pawn) {
                            // Penalize if the next three cells are occupied by a black pawn
                            if (colorOfBot.equals("black")) {
                                value -= 0.15;
                            }
                        }
                    }
                }
            }
        }

        return value;
    }


    /**
     * Evaluates the position and safety of the king on the board.
     * Positive values indicate a better position for the bot's king, while negative values indicate
     * a better position for the opponent's king. The evaluation also considers the safety of the kings
     * based on surrounding pieces.
     *
     * @param colorOfBot The color of the bot ('white' or 'black').
     * @param figures     The current configuration of chess figures on the board.
     * @return A value indicating the position and safety advantage/disadvantage for the bot's king.
     */
    static double positionAndSafetyOfKing(String colorOfBot, Figure[][] figures) {
        double value = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (figures[i][j] != null && figures[i][j] instanceof King) {
                    if (i >= 1 && i < 7 && j >= 1 && j < 7) {
                        if (colorOfBot == figures[i][j].color) {
                            value -= 0.2;
                        } else {
                            value += 0.2;
                        }
                    }
                    if (i >= 2 && i < 6 && j >= 2 && j < 6) {
                        if (colorOfBot == figures[i][j].color) {
                            value -= 0.2;
                        } else {
                            value += 0.2;
                        }
                    }
                    if (i >= 6 && j >= 6) {
                        if (colorOfBot == figures[i][j].color) {
                            value += 0.1;
                        } else {
                            value -= 0.1;
                        }
                    }
                    if (i >= 6 && j <= 1) {
                        if (colorOfBot == figures[i][j].color) {
                            value += 0.2;
                        } else {
                            value -= 0.2;
                        }
                    }
                    if (i <= 1 && j <= 1) {
                        if (colorOfBot == figures[i][j].color) {
                            value += 0.2;
                        } else {
                            value -= 0.2;
                        }
                    }
                    if (i <= 1 && j >= 6) {
                        if (colorOfBot == figures[i][j].color) {
                            value += 0.2;
                        } else {
                            value -= 0.2;
                        }
                    }
                    int count = 0;
                    if (i - 1 >= 0 && i - 1 < 8 && j + 1 >= 0 && j + 1 < 8 && figures[i - 1][j + 1] != null
                            && figures[i - 1][j + 1].color == figures[i][j].color) {
                        count++;
                    }
                    if (i >= 0 && i < 8 && j + 1 >= 0 && j + 1 < 8 && figures[i][j + 1] != null
                            && figures[i][j + 1].color == figures[i][j].color) {
                        count++;
                    }
                    if (i + 1 >= 0 && i + 1 < 8 && j + 1 >= 0 && j + 1 < 8 && figures[i + 1][j + 1] != null
                            && figures[i + 1][j + 1].color == figures[i][j].color) {
                        count++;
                    }
                    if (i - 1 >= 0 && i - 1 < 8 && j - 1 >= 0 && j - 1 < 8 && figures[i - 1][j - 1] != null
                            && figures[i - 1][j - 1].color == figures[i][j].color) {
                        count++;
                    }
                    if (i >= 0 && i < 8 && j - 1 >= 0 && j - 1 < 8 && figures[i][j - 1] != null
                            && figures[i][j - 1].color == figures[i][j].color) {
                        count++;
                    }
                    if (i + 1 >= 0 && i + 1 < 8 && j - 1 >= 0 && j - 1 < 8 && figures[i + 1][j - 1] != null
                            && figures[i + 1][j - 1].color == figures[i][j].color) {
                        count++;
                    }
                    if (count >= 2) {
                        if (colorOfBot == figures[i][j].color) {
                            value += 0.4;
                        } else {
                            value -= 0.4;
                        }
                    }
                }
            }
        }
        return value;
    }


    /**
     * Evaluates the material on the board, considering the value of chess figures.
     * Positive values indicate an advantage for the bot's color, while negative values indicate
     * an advantage for the opponent's color.
     *
     * @param colorOfBot The color of the bot ('white' or 'black').
     * @param figures     The current configuration of chess figures on the board.
     * @return A value indicating the material advantage/disadvantage for the bot.
     */
    static double material(String colorOfBot, Figure[][] figures) {
        double value = 0;

        // Iterate over each cell on the board
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                // Check if the cell contains a chess figure
                if (figures[i][j] != null) {
                    // Adjust the material value based on the color of the chess figure
                    if (figures[i][j].color.equals(colorOfBot))
                        value += figures[i][j].value;
                    else {
                        value -= figures[i][j].value;
                    }
                }
            }
        }

        return value;
    }


    /**
     * Evaluates the pawn structure on the board, considering the positioning of pawns and interactions.
     * The value is adjusted based on pawn structure, such as doubled pawns and pawn attacks.
     *
     * @param colorOfBot The color of the bot ('white' or 'black').
     * @param figures     The current configuration of chess figures on the board.
     * @return A value indicating the evaluation of the pawn structure.
     */
    static double pawnStructure(String colorOfBot, Figure[][] figures) {
        double value = 0;

        // Iterate over each cell on the board
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                // Check if the cell contains a pawn
                if (figures[i][j] != null && figures[i][j] instanceof Pawn) {
                    // Check for interactions with adjacent cells based on pawn color
                    if (figures[i][j].color.equals("white")) {
                        if (i + 1 >= 0 && i + 1 < 8 && j + 1 >= 0 && j + 1 < 8 && figures[i + 1][j + 1] != null) {
                            if (figures[i + 1][j + 1].color.equals("white")
                                    && figures[i + 1][j + 1] instanceof Pawn) {
                                if (colorOfBot.equals("white")) {
                                    value += 0.1;
                                } else {
                                    value -= 0.1;
                                }
                            }
                        }
                        if (i - 1 >= 0 && i - 1 < 8 && j + 1 >= 0 && j + 1 < 8 && figures[i - 1][j + 1] != null) {
                            if (figures[i - 1][j + 1].color.equals("white")
                                    && figures[i - 1][j + 1] instanceof Pawn) {
                                if (colorOfBot.equals("white")) {
                                    value += 0.1;
                                } else {
                                    value -= 0.1;
                                }
                            }
                        }
                    } else if (figures[i][j].color.equals("black")) {
                        if (i + 1 >= 0 && i + 1 < 8 && j - 1 >= 0 && j - 1 < 8 && figures[i + 1][j - 1] != null) {
                            if (figures[i + 1][j - 1].color.equals("black")
                                    && figures[i + 1][j - 1] instanceof Pawn) {
                                if (colorOfBot.equals("black")) {
                                    value += 0.1;
                                } else {
                                    value -= 0.1;
                                }
                            }
                        }
                        if (i - 1 >= 0 && i - 1 < 8 && j - 1 >= 0 && j - 1 < 8 && figures[i - 1][j - 1] != null) {
                            if (figures[i - 1][j - 1].color.equals("black")
                                    && figures[i - 1][j - 1] instanceof Pawn) {
                                if (colorOfBot.equals("black")) {
                                    value += 0.1;
                                } else {
                                    value -= 0.1;
                                }
                            }
                        }
                    }
                }
            }
        }

        return value;
    }


    /**
     * Evaluates the color distribution of pawns on the board based on the position of bishops.
     * The value is adjusted depending on the presence and color of bishops.
     *
     * @param colorOfBot The color of the bot ('white' or 'black').
     * @param figures     The current configuration of chess figures on the board.
     * @return A value indicating the evaluation of pawn color distribution.
     */
    static double colorOfPawns(String colorOfBot, Figure[][] figures) {
        double value = 0;
        int bishopCounter = 0;

        // Count the number of bishops of the opposite color
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (figures[i][j] != null && figures[i][j].color != colorOfBot) {
                    if (figures[i][j] instanceof Bishop && figures[i][j].bishopColor.equals("white")) {
                        bishopCounter += 1;
                    } else {
                        bishopCounter -= 1;
                    }
                }
            }
        }

        // Adjust the value based on the bishop count and pawn colors
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (figures[i][j] != null && figures[i][j].color.equals(colorOfBot)) {
                    if (bishopCounter >= 1) {
                        if ((i + j) % 2 != 0) {
                            value += 0.13;
                        }
                    }
                    if (bishopCounter <= -1) {
                        if ((i + j) % 2 == 0) {
                            value += 0.13;
                        }
                    }
                }
            }
        }

        return value;
    }


    /**
     * Checks if there is a checkmate in the current game state.
     *
     * @param colorOfBot           The color of the bot ('white' or 'black').
     * @param figures              The current configuration of chess figures on the board.
     * @param positionCountMapWhite Map to count positions for white pieces.
     * @param positionCountMapBlack Map to count positions for black pieces.
     * @return 10000 if checkmate for the opponent, -10000 if checkmate for the bot, 1 if stalemate, 0 otherwise.
     */
    static int checkmate(String colorOfBot, Figure[][] figures, Map<String, Integer> positionCountMapWhite,
            Map<String, Integer> positionCountMapBlack) {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (figures[i][j] != null && figures[i][j] instanceof King) {
                    King king = (King) figures[i][j];
                    String x = king.checkmate(Main.fiftyMoveCounter, figures, positionCountMapWhite,
                            positionCountMapBlack);
                    if (x == "y") {
                        if (colorOfBot != king.color) {
                            return 10000;
                        } else {
                            return -10000;
                        }
                    }
                    if (x == "d") {
                        return 1;
                    }
                }
            }
        }
        return 0;
    }
}
