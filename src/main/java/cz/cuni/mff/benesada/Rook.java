package cz.cuni.mff.benesada;

/**
 * Represents a Rook chess piece.
 */
public class Rook extends Figure {

    /**
     * Constructor for the Rook class.
     *
     * @param positionX The initial X-coordinate of the Rook on the chessboard.
     * @param positionY The initial Y-coordinate of the Rook on the chessboard.
     * @param color The color of the Rook ("white" or "black").
     */
    public Rook(int positionX, int positionY, String color) {
        super(positionX, positionY, color, "rook");
        super.value = 5; // Setting the value of the Rook piece.
    }

    /**
     * Moves the Rook to a new position and prints the move details.
     *
     * @param newPositionX The new X-coordinate for the Rook.
     * @param newPositionY The new Y-coordinate for the Rook.
     * @param wasTherePromotion A string indicating if there was a pawn promotion during the move.
     * @return The result of the move operation.
     */
    @Override
    public String move(int newPositionX, int newPositionY, String wasTherePromotion) {
        System.out.println("Rook moves to (" + newPositionX + ", " + newPositionY + ")");
        return super.move(newPositionX, newPositionY, wasTherePromotion);
    }
}
