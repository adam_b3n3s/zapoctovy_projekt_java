package cz.cuni.mff.benesada;

/**
 * Represents a Bishop chess piece that extends the Figure class.
 */
public class Bishop extends Figure {
    
    /**
     * The color of the bishop. It is a static variable shared among all instances.
     */
    public static String bishopColor;
    
    /**
     * Constructs a Bishop object with the specified position, color, and bishop color.
     *
     * @param positionX The X-coordinate of the bishop's position on the chessboard.
     * @param positionY The Y-coordinate of the bishop's position on the chessboard.
     * @param color The color of the bishop (e.g., "white" or "black").
     * @param bishopColor The specific color of the bishop (e.g., "light" or "dark").
     */
    public Bishop(int positionX, int positionY, String color, String bishopColor) {
        super(positionX, positionY, color, "bishop");
        super.bishopColor = bishopColor;
        super.value = 3;
    }

    /**
     * Moves the bishop to the specified position and prints the move information.
     *
     * @param newPositionX The new X-coordinate for the bishop's position.
     * @param newPositionY The new Y-coordinate for the bishop's position.
     * @param wasTherePromotion A string indicating whether there was a pawn promotion during the move.
     * @return The result of the move, including information about promotion if applicable.
     */
    @Override
    public String move(int newPositionX, int newPositionY, String wasTherePromotion) {
        System.out.println("Bishop moves to (" + newPositionX + ", " + newPositionY + ")");
        return super.move(newPositionX, newPositionY, wasTherePromotion);
    }
    
}
